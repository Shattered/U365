TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \ 
    src/arch/i686/div64.c \
    src/arch/i686/init.c \
    src/ata.c \
    src/cmds.c \
    src/debug.c \
    src/devices.c \
    src/drawing.c \
    src/fbuf.c \
    src/file_Font_bfn.c \
    src/filesystem.c \
    src/fio.c \
    src/font.c \
    src/heap.c \
    src/init.c \
    src/ish.c \
    src/main.c \
    src/memory.c \
    src/panic.c \
    src/ports.c \
    src/reboot.c \
    src/smbios.c \
    src/stdio.c \
    src/string.c \
    src/surface.c \
    src/task.c \
    src/vector.c \
    src/vfs.c \
    src/vfs_node.c \
    src/glyph.c \
    src/arch/i686/acpi.c \
    src/bmp.c \
    src/file_bmp_bmp.c \
    src/file_logo_bmp.c \
    src/string_tree.c \
    vfs/tree/fonts/file_vfs_tree_fonts_Font_bfn.c \
    vfs/tree/file_vfs_tree_bmp_bmp.c \
    vfs/tree/file_vfs_tree_fonts_Font_bfn.c \
    file_vfs_tree_U365_bmp.c \
    src/arch/i686/cmos_rtc.c \
    src/arch/i686/gdt.c \
    src/arch/i686/kbd.c \
    src/tty_old.c \
    src/arch/i686/idt.c \
    src/arch/i686/pic.c \
    src/arch/i686/cpu.c

HEADERS += \
    include/cmds.h \
    include/compare.h \
    include/debug.h \
    include/drawing.h \
    include/file_Font_bfn.h \
    include/filesystem.h \
    include/fio.h \
    include/init.h \
    include/ish.h \
    include/memory.h \
    include/panic.h \
    include/shell.h \
    include/stdio.h \
    include/string.h \
    include/surface.h \
    include/sys.h \
    include/task.h \
    include/time.h \
    include/vector.h \
    include/vesa_fb.h \
    include/vfs.h \
    include/vfs_node.h \
    include/glyph.h \
    include/font.h \
    include/bmp.h \
    include/file_bmp_bmp.h \
    include/file_logo_bmp.h \
    include/string_tree.h \
    include/vfs_tree.h \
    vfs/tree/fonts/file_vfs_tree_fonts_Font_bfn.h \
    vfs/tree/file_vfs_tree_bmp_bmp.h \
    vfs/tree/file_vfs_tree_fonts_Font_bfn.h \
    file_vfs_tree_U365_bmp.h \
    include/thread.h \
    include/segment.h \
    include/arch/i686/acpi.h \
    include/arch/i686/asmdefs.h \
    include/arch/i686/ata.h \
    include/arch/i686/devices.h \
    include/arch/i686/gdt.h \
    include/arch/i686/io.h \
    include/arch/i686/mboot.h \
    include/arch/i686/idt.h \
    include/arch/i686/kbd.h \
    include/arch/i686/paging.h \
    include/tty_old.h

DISTFILES += \
    Font.bfn \
    U365.png \
    link.ld \
    Makefile \
    src/arch/i686/boot.s \
    src/arch/i686/task_switch.s \
    src/arch/i686/gdt.asm \
    src/arch/i686/idt.asm
