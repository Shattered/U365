#include "../include/debug.h"

#include <stdarg.h>

#include "../include/stdio.h"
#include "../include/arch/i686/asmdefs.h"

const char *ferrs    [64] = {
    "Your PC isn't compatible with U365. Sorry.",
    "ACPI: RSDP signature invalid",
    "ACPI: RSDP checksum invalid",
    "ACPI: RSDT invalid",
    "ACPI: FADT not found",
    "ACPI: Error while DSDT parsing: Invalid DSDT signature",
	"ACPI: Error while DSDT parsing: No FADT (maybe parse_dsdt() is called before FADT was found)",
    "ACPI: Cannot enable ACPI.",
    "ACPI: RSDT signature is valid, but checksum is wrong. Aborting.",
    "ACPI: No \\_S5 object in DSDT.",
    "FATAL BOOT ERROR: Invalid Multiboot magic number (EAX magic).",
    "No initrd was found (maybe you didn't used the \"initrd\" parameter to the module?)",
    "kernel_main() exited, halting.",
    "FATAL BOOT ERROR: Incompatible bootloader. GRUB 2 is the only officially supported bootloader - use it to avoid boot errors.",
};

int errorlevel=3;   //Use 3 for release, 2 for development and 0 for debugging.
                    //Why not 16 for release? That's because user DOES need
                    //to see error messages. 3 will disable only success, ini-
                    //tialization, debug and notice messages. Warnings, errors,
                    //fatal errors and panics still will be visible.
const char *notices  [16] = {"[NOTICE ] ", "[  INIT ] ", "[ DEBUG ] ", "[SUCCESS] ", "[WARNING] ", "[ FATAL ] ", "[ ERROR ] ", "[ PANIC ] "};
uint8_t notcolors[16] = {0x09, 0x06, 0x03, 0x02, 0x0e, 0x04, 0x04, 0x0c};
void debug_print(int lvl, const char* str)
{
    if(lvl<=errorlevel)
    	return;
   	tty_setcolor(notcolors[lvl]);
    tty_wrstr(notices[lvl]);
    tty_setcolor(0x07);
    tty_wrstr(str);
    tty_wrstr("\n");
}

void fatal_error(int msg)
{
	debug_print(PANIC,ferrs[msg]);
	INT_OFF;
	HALT   ;
}
