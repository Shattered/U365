//#include "../include/arch/i686/mboot.h"
#include "../include/debug.h"
#include "../include/vesa_fb.h"
#include "../include/ish.h"
#include "../include/memory.h"
#include "../include/stdio.h"
#include "../include/sys.h"
#include "../include/arch/i686/regs.h"
#include "../include/tty_old.h"
#include "../include/time.h"
#include "../include/init.h"
#include "../include/surface.h"
#include "../include/string.h"
#include "../include/compare.h"
#include "../include/arch/i686/devices.h"
#include "../include/bmp.h"
/*
 * U365 Kernel entry. Called by _start, doing pre-run tasks and running the shell.
 */

void kernel_main(int eax_magic, struct mboot_info *mboot)
{
    
    init_srtInfo();
    initVBE(mboot);
    initTTY();
    tty_wrstr("U365 " QUOT(OSVER) " kernel is loaded.\nChecking bootloader magic number...");
    //If the EAX magic number passed by GRUB isn't valid, display error and stop boot process.
    if(eax_magic!=0x2BADB002)
    {
        tty_putchar('\n');
        fatal_error(EBOOT_MAGIC_ERROR);
    }
    tty_wrstr("OK.\n");
    // printf() uses memory allocating, which will be initialized later
    #ifdef NOT_FOR_RELEASE
    debug_print(WARNING, "This version is an unstable version from Git. Not for daily use, only for BPS developers.");
    #endif
    tty_putchar('\n');
    //Run pre-run tasks and start the shell.
    os_lowlevel_init(mboot);
    debug_print(SUCCESS,"OS is initialized. Loading the shell...");
    ishMain();
    fatal_error(EKMAIN_EXITED); //crash if ISH returned.
}

