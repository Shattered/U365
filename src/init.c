#include "../include/init.h"

#include <stddef.h>
#include "../include/debug.h"
#include "../include/arch/i686/devices.h"
#include "../include/arch/i686/io.h"
#include "../include/arch/i686/mboot.h"
#include "../include/arch/i686/smbios.h"

#include "../include/tty_old.h"
#include "../include/stdio.h"
#include "../include/vfs.h"
#include "../include/surface.h"
#include "../include/vesa_fb.h"

// Initializing functions are only used in there, so we shouldn't declare them in their headers
void   arch_init();
void   init_stdio();
void   init_rootfs();
size_t init_memory();
void   setupPIT();
void   mouse_install();
void ide_initialize(unsigned int BAR0, unsigned int BAR1, unsigned int BAR2, unsigned int BAR3, unsigned int BAR4);
bool __u365_init_done=false;

void os_lowlevel_init(struct mboot_info *mboot)
{
    debug_print(INIT,"init: Initializing memory management...");
    init_memory();

    debug_print(INIT,"init: Initializing STDIO...");
    init_stdio();
    printf("Checking minimum system requirements.\n");
    if((mboot->mem_upper+mboot->mem_lower)/1024+1<32)
    {
        printf("Non-compatible PC. Minimal RAM amount for U365 is 32 MB, but you have only %d MB.\n", (mboot->mem_upper+mboot->mem_lower)/1024+1);
        fatal_error(ENOTCOMPAT);
    }
    printf("Boot info:\nBooted by Multiboot-compilant bootloader \"%s\" "
    "from BIOS device %d, "
    "command line \"%s\", "
    "%u MB of RAM\n", mboot->boot_loader_name,
    ((char*)mboot->boot_device)[0],
    mboot->cmdline,
    (mboot->mem_upper+mboot->mem_lower)/1024+1);
    debug_print(INIT,"init: Initializing architecture-specific features\n");
    arch_init(); //Initialize architecture-specific features.
    tty_putchar('\n');
    debug_print(INIT,"init: Screen surface");
    init_surface_screen();

    mouse_install();
    
    debug_print(INIT,"init: Initializing PIT");
    debug_print(INIT,"init: Detecting CPU speed. It may take up to 1 second");
    cpuMHZ=getCPUFreq();

    debug_print(INIT,"init: Finding initrd");
    struct mboot_mod_list_struct* mboot_mod_list;
    mboot_mod_list = (struct mboot_mod_list_struct*) (uintptr_t) mboot->mods_addr;
    unsigned int i;
    for (i = 0; i <= (mboot->mods_count); i++)
    {
        if(strcmp((const char *)mboot_mod_list->cmdline, "initrd")==0)
        {
            initrd_address=(uint8_t*)(uintptr_t) mboot_mod_list->mod_start;
            break;
        }
        mboot_mod_list += 1;
    }
    if(i>=mboot->mods_count)
    {
        fatal_error(ENO_INITRD);
    }

    debug_print(INIT,"init: Initializing VFS");
    init_rootfs();
    //Setting the __u365_init_done variable, so some functions can know that we're not in boot process.
    __u365_init_done = true;
    //debug_print(NOTICE,"init: Initializing ATA...");
    //ide_initialize(0x1F0, 0x3F4, 0x170, 0x374, 0x000);
    debug_print(SUCCESS,"init: All work is done, exiting.");
    return;
}

