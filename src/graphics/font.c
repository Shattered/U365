#include "../include/font.h"

#include "../include/string.h"
#include "../include/surface.h"
#include "../include/memory.h"

/**
 * Basic class operations
 */
font* font_new()
{
    font* this = malloc(sizeof(font));
    if(this && font_constructor(this))
        return this;
    return 0;
}

void font_delete(font* this)
{
    font_destructor(this);
    free(this);
}

uint8_t font_constructor(font* this)
{
    this->g = 0;
    this->w = 0;
    this->h = 0;
    this->n = 0;
    return 1;
}

void font_destructor(font* this)
{
    size_t i;
    for(i = 0; i < this->n; ++i)
    {
        glyph_destructor(this->g + i);
    }
    free(this->g);
}

/**
 * Methods
 */
font* font_open(FILE* f)
{
    font* this = font_new();
    fread(&this->n, 4, 1, f);
    fread(&this->w, 4, 1, f);
    fread(&this->h, 4, 1, f);
    this->g = malloc(sizeof(glyph) * this->n);
    size_t i;
    for(i = 0; i < this->n; ++i)
    {
        glyph_read(this->g + i, f, this->w, this->h);
    }
    return this;
}

surface* font_draw_unicode(font* this, uint32_t* t, color_rgba c)
{
    size_t i;
    size_t j;
    size_t n;
    surface* s = surface_new();
    for(n = 0; t[n]; ++n);
    surface_default_init(s, this->w * n, this->h);
    rect r = {0, 0, this->w, this->h};
    for(i = 0; i < n; ++i)
    {
        r.x = i * this->w;
        glyph* g = 0;
        for(j = 0; j < this->n; ++j)
        {
            if(this->g[j].c == t[i])
            {
                g = this->g + j;
                break;
            }
        }
        if(!g)
        {
            printf("font_draw: glyph [0x%04x] not found\n", t[i]);
            g = this->g;
        }
        glyph_draw(g, s, c, r);
    }
    return s;
}

size_t utf8_convert(const char* text, uint32_t* out)
{
    size_t i, j, n = strlen(text) + 1, k, m = 0;
    uint32_t buf;
    uint8_t mask1 = 0;
    for(i = 0; text[i]; ++i)
    {
        buf = 0;
        mask1 = 0b01111111;
        k = 0;
        if((text[i] & 0b11000000) == 0b11000000)
        {
            for(k = 1; ((text[i] << k) & 0b10000000) && k <= 6; ++k)
            {
                mask1 = mask1 >> 1;
            }
        }
        if(k)
            --k;
        buf = (((uint32_t)(text[i])) & mask1) << (k * 6);
        j = i + k;
        for(; i < j && i < n - 1; ++i)
        {
            buf |= (((uint32_t)(text[i + 1]) & 0b111111) << ((j - i - 1) * 6));
        }
        out[m++] = buf;
    }
    out[m] = 0;
    return m;
}
surface* bfn_write_str(const char* s, color_rgba color)
{
    FILE* fi = fopen("/fonts/Font.bfn", "r");
    font* fo = font_open(fi);
    uint32_t* tt = (uint32_t*)(calloc(strlen(s) + 1, 4));
    utf8_convert(s, tt);
    surface* sf=surface_new();
    sf = font_draw_unicode(fo, tt, color);
    return sf;
}