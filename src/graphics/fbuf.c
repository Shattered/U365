#include <stdint.h>
#include "../include/arch/i686/mboot.h"
#include "../include/vesa_fb.h"
#include "../include/arch/i686/devices.h"
#include "../include/surface.h"

//VBE Linear frame buffer small driver.

void putPix(int x,int y,uint32_t color)
{
    color_rgba col;
    col.r = color >> 16;
    col.g = color >> 8;
    col.b = color;
    col.a = 0xFF;
    if(surface_screen)
    {
        surface_set_pixel(surface_screen, col, x, y);
    }
    else
    {
        if(x>=srt_info.ScreenFBInfo.width || y>=srt_info.ScreenFBInfo.height)
            return;
        srt_info.ScreenFBInfo.pixbuf[y][x]=color;
        unsigned where = x*(srt_info.ScreenFBInfo.fbbpp/8) + y*srt_info.ScreenFBInfo.fbpitch;
        srt_info.ScreenFBInfo.framebuffer[where + 0] = col.b;
        srt_info.ScreenFBInfo.framebuffer[where + 1] = col.g;
        srt_info.ScreenFBInfo.framebuffer[where + 2] = col.r;
    }
}
uint32_t getPix(int x, int y)
{
	return srt_info.ScreenFBInfo.pixbuf[y][x];
}
/*
Initializing VESA mode. You'll need a Multiboot struct to work, because it's using info from it.
*/
void initVBE(struct mboot_info *mboot)
{
    //First, get the LFB address from MB info struct.
    srt_info.ScreenFBInfo.framebuffer = (uint8_t*)(int)(mboot->framebuffer_addr);
    //Then get the pitch.
    srt_info.ScreenFBInfo.fbpitch=mboot->framebuffer_pitch;
    //Then BPP.
    srt_info.ScreenFBInfo.fbbpp=mboot->framebuffer_bpp;
    //And, finally, screen width and height.
    srt_info.ScreenFBInfo.width = mboot->framebuffer_width ;
    srt_info.ScreenFBInfo.height = mboot->framebuffer_height;
}
