#include "../include/surface.h"

#include "../include/vesa_fb.h"
#include "../include/arch/i686/devices.h"
#include "../include/memory.h"
#include "../include/compare.h"

const color_rgba color_transparency = {0x00, 0x00, 0x00, 0x00};
const color_rgba color_black        = {0x00, 0x00, 0x00, 0xFF};
const color_rgba color_red          = {0xFF, 0x00, 0x00, 0xFF};
const color_rgba color_green        = {0x00, 0xFF, 0x00, 0xFF};
const color_rgba color_blue         = {0x00, 0x00, 0xFF, 0xFF};
const color_rgba color_cyan         = {0x00, 0xFF, 0xFF, 0xFF};
const color_rgba color_magenta      = {0xFF, 0x00, 0xFF, 0xFF};
const color_rgba color_yellow       = {0xFF, 0xFF, 0x00, 0xFF};
const color_rgba color_white        = {0xFF, 0xFF, 0xFF, 0xFF};

surface* surface_screen = 0;

color_rgba color_split(color_rgba a, color_rgba b)
{
    color_rgba result;
    if(a.a==0)return b;
    if(b.a==0)return a;
    if(a.a>=127)return b;
    if(b.a>=127)return a;
    result.r = ( a.r * ((a.a << 8) + a.a) + b.r * ( ((a.a ^ 0xFF) << 8) + (a.a ^ 0xFF) ) ) >> 16;
    result.g = ( a.g * ((a.a << 8) + a.a) + b.g * ( ((a.a ^ 0xFF) << 8) + (a.a ^ 0xFF) ) ) >> 16;
    result.b = ( a.b * ((a.a << 8) + a.a) + b.b * ( ((a.a ^ 0xFF) << 8) + (a.a ^ 0xFF) ) ) >> 16;
    result.a = ( a.a * ((a.a << 8) + a.a) + b.a * ( ((a.a ^ 0xFF) << 8) + (a.a ^ 0xFF) ) ) >> 16;
    return result;
}

color_rgba color_uint32_to_rgba(uint32_t d)
{
    color_rgba result;
    result.r = d >> 6;
    result.g = (d >> 4) & 0xFF;
    result.b = (d >> 2) & 0xFF;
    result.a = d & 0xFF;
    return result;
}

uint32_t color_rgba_to_uint32(color_rgba c)
{
    return ((c.r << 6) | (c.g << 4) | (c.b << 2) | (c.a));
}

/**
 * Basic class operations
 */
surface* surface_new()
{
    surface* this = malloc(sizeof(surface));
    if(this && surface_constructor(this))
        return this;
    return 0;
}

void surface_delete(surface* this)
{
    if(this)
    {
        if(this->del)
            this->del(this);
        free(this);
    }
}

uint8_t surface_constructor(surface* this)
{
    this->data  = 0;
    this->w     = 0;
    this->h     = 0;
    this->flags = 0;
    this->rb    = 0;
    this->gb    = 0;
    this->bb    = 0;
    this->ab    = 0;
    return 1;
}

/**
 * Methods
 */
surface* surface_copy(const surface* this)
{
    rect r = {0, 0, this->w, this->h};
    return surface_extract(this, r);
}

uint8_t surface_apply(surface* this, const surface* src, rect r)
{
    if(!this || !src || !(this->flags & SURFACE_EDITABLE))
        return 0;
    uint32_t i, j;

    for(i = 0; i < r.h && i + r.y < this->h; ++i)
    {
        for(j = 0; j < r.w && j < r.x + this->w; ++j)
        {
            color_rgba tmp = color_split(surface_get_pixel(this, r.x + j, r.y + i), surface_get_pixel(src, j, i));
            surface_set_pixel(this, tmp, r.x + j, r.y + i);
        }
    }
    return 1;
}

surface* surface_extract(const surface* this, rect r)
{
    if(!this || !r.w || !r.h)
        return 0;
    surface* result = surface_new();
    surface_default_init(result, r.w, r.h);
    uint32_t i, j;
    for(i = 0; i < r.h && i < r.y + this->h; ++i)
    {
        for(j = 0; j < r.w && j < r.x + this->w; ++j)
        {
            surface_set_pixel(result, surface_get_pixel(this, r.x + j, r.y + i), j, i);
        }
    }
    return result;
}

uint8_t surface_fill_rect(surface* this, color_rgba color, rect r)
{
    if(!this || !(this->flags & SURFACE_EDITABLE))
        return 0;
    uint32_t i, j;
    for(i = r.y; i < r.y + r.h; ++i)
    {
        for(j = r.x; j < r.x + r.w; ++j)
        {
            surface_set_pixel(this, color, j, i);
        }
    }
    return 1;
}

uint8_t surface_resize(surface* this, uint32_t w, uint32_t h)
{
    if(!this || !(this->flags & SURFACE_RESIZABLE))
        return 0;
    uint32_t i;
    this->data = realloc(this->data, this->bpp * h);
    if(!this->data)
        return 0;

    for(i = this->h; i < h; ++i)
        this->data[i] = 0;
    for(i = 0; i < h; ++i)
    {
        this->data[i] = realloc(this->data[i], w * this->bpp);
        if(!this->data[i])
            return 0;
    }
    this->w = w;
    this->h = h;
    return 1;
}

uint8_t surface_set_pixel(surface* this, color_rgba color, uint32_t x, uint32_t y)
{
    if(this && this->flags & SURFACE_EDITABLE && x < this->w && y < this->h)
    {
        if(this->rb < this->bpp) ((uint8_t**)(this->data))[y][x * this->bpp + this->rb] = color.r;
        if(this->gb < this->bpp) ((uint8_t**)(this->data))[y][x * this->bpp + this->gb] = color.g;
        if(this->bb < this->bpp) ((uint8_t**)(this->data))[y][x * this->bpp + this->bb] = color.b;
        if(this->flags & SURFACE_ALPHA && this->ab < this->bpp)
            ((uint8_t**)(this->data))[y][x * this->bpp + this->ab] = color.a;
        return 1;
    }
    return 0;
}

color_rgba surface_get_pixel(const surface* this, uint32_t x, uint32_t y)
{
    color_rgba result = {0, 0, 0, 0};
    if(this->flags & SURFACE_READABLE && x < this->w && y < this->h)
    {
        if(this->rb < this->bpp) result.r = ((uint8_t**)(this->data))[y][x * this->bpp + this->rb];
        if(this->gb < this->bpp) result.g = ((uint8_t**)(this->data))[y][x * this->bpp + this->gb];
        if(this->bb < this->bpp) result.b = ((uint8_t**)(this->data))[y][x * this->bpp + this->bb];
        if(this->flags & SURFACE_ALPHA && this->ab < this->bpp)
            result.a = ((uint8_t**)(this->data))[y][x * this->bpp + this->ab];
        else
            result.a = 255;
    }
    return result;
}

/**
 * Stuff
 */
// Default destructor to be set which will free all data allocated for image holding
void surface_default_destructor(surface* this)
{
    uint32_t i;
    for(i = 0; i < this->h; ++i)
    {
        free(this->data[i]);
    }
    free(this->data);
}
// Initialize all data fields for ordinary RGBA image holding
void surface_default_init(surface* this, uint32_t w, uint32_t h)
{
    this->data  = malloc(sizeof(void*) * h);
    this->w     = w;
    this->h     = h;
    this->flags = SURFACE_DEFAULT_FLAGS;
    this->rb    = 3;
    this->gb    = 2;
    this->bb    = 1;
    this->ab    = 0;
    this->bpp   = 4;
    this->del   = &surface_default_destructor;
    uint32_t i;
    for(i = 0; i < h; ++i)
    {
        this->data[i] = calloc(w * 4, 1);
    }
}

/**
 * Screen surface
 */
void surface_screen_del(surface* this)
{
    free(this->data);
}

// Initialize or reinitialize the screen surface pointer
// Please call when the screen definition is changed
void init_surface_screen()
{
    if(surface_screen)
        surface_delete(surface_screen);

    surface_screen        = surface_new();
    surface_screen->flags = SURFACE_READABLE | SURFACE_EDITABLE;
    surface_screen->del   = &surface_screen_del;

    int i;
    int w = srt_info.ScreenFBInfo.width;
    int h = srt_info.ScreenFBInfo.height;
    void* scrbuf = srt_info.ScreenFBInfo.framebuffer;

    surface_screen->data = malloc(h * sizeof(void*));
    surface_screen->w = w;
    surface_screen->h = h;
    surface_screen->bpp = srt_info.ScreenFBInfo.fbbpp / 8;
    surface_screen->rb = 2;
    surface_screen->gb = 1;
    surface_screen->bb = 0;
    if(surface_screen->data)
    for(i = 0; i < h; ++i)
    {
        surface_screen->data[i] = (uint8_t*)(scrbuf) + i * srt_info.ScreenFBInfo.fbpitch;
    }
}
void surface_put_line (surface* ret, rect r, color_rgba color)
{
    //It's not my code, copied from Web.
    const int dx = max(r.w, r.x)-min(r.w, r.x);
    const int dy = max(r.h, r.y)-min(r.h, r.y);
    const float de = dy;
    float error = 0;
    int y=max(r.h, r.y);
    for(int x=min(r.w, r.x);x<max(r.w, r.x);x++){
        surface_set_pixel(ret,color,x,y);
        error += de;
            if(2 * error >= dx){
                y++;
                error -= dx;
            }
    }
}