#include <vesa_fb.h>
#include <surface.h>
#include <compare.h>

//Drawing using VESA framebuffer.
void hLine(int x, int y, int length, uint32_t color)
{
	for(int i=0; i < length; i++)
	{
		putPix(x++,y,color);
	}
}
void vLine(int x, int y, int length, uint32_t color)
{
	for(int i=0; i < length; i++)
	{
		putPix(x,y++,color);
	}
}
void drawCircle(int x1, int y1, int radius, uint32_t color)
{
	int x=0, y=radius, delta=1-2*radius, error=0;
	while(y>=0)
	{
		putPix(x1+x,y1+y,color);
		putPix(x1+x,y1-y,color);
		putPix(x1-x,y1+y,color);
		putPix(x1-x,y1-y,color);
		error = 2 * (delta + y) - 1;
		 if ((delta < 0) && (error <= 0))
		 {
           delta += 2 * ++x + 1;
           continue;
		 }
		error = 2 * (delta - x) - 1;
		if ((delta > 0) && (error > 0))
		{
        	delta += 1 - 2 * --y;
        	continue;
    	}
    	       x++;
       delta += 2 * (x - y);
       y--;
	}
}
void fillRect(int x_, int y_, int w, int h, uint32_t color)
{
	for(int i=0; i<h; i++)
		for(int j=0; j<w; j++)
			putPix(x_+j,y_+i,color);
}

void drawBitmap(int x, int y, uint32_t bitmap[], int w, int h)
{
	for(int i=0; i<h; i++)
		for(int j=0; j<w; j++)
			if(bitmap[i*w+j]!=0x000000)
			putPix(x+j,y+i,bitmap[i*w+j]);
}