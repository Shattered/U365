#include "../include/glyph.h"

#include "../include/memory.h"
#include "../include/vfs.h"

/**
 * Basic class operations
 */
glyph* glyph_new()
{
    glyph* this = malloc(sizeof(glyph));
    if(this && glyph_constructor(this))
        return this;
    return 0;
}

void glyph_delete(glyph* this)
{
    if(this)
    {
        glyph_destructor(this);
        free(this);
    }
}

uint8_t glyph_constructor(glyph* this)
{
    this->data = 0;
    this->w = 0;
    this->h = 0;
    this->c = 0;
    return 1;
}

void glyph_destructor(glyph* this)
{
    size_t i;
    for(i = 0; i < this->h; ++i)
    {
        free(this->data[i]);
    }
    free(this->data);
}

/**
 * Methods
 */
void glyph_read(glyph* this, FILE* f, uint32_t w, uint32_t h)
{
    this->w = w;
    this->h = h;
    uint32_t i, j;
    this->data = malloc(h * sizeof(uint8_t*));
    fread(&(this->c), 4, 1, f);
    fread(&(this->o), 4, 1, f);
    for(i = 0; i < h; ++i)
    {
        this->data[i] = malloc(w);
        for(j = 0; j < w; ++j)
        {
            int16_t r = fgetc(f);
            if(r == -1)
            {
                printf("Font file is corrupted (%i, %i, %i)\n", this->c, j, i);
                return;
            }
            this->data[i][j] = r;
        }
    }
}

void glyph_draw(glyph* this, surface* target, color_rgba c, rect r)
{
    uint32_t i, j;
    for(i = 0; i < this->h && i < r.h && r.y + i < target->h; ++i)
    {
        for(j = 0; j < this->w && j < r.w && r .x + j < target->w; ++j)
        {
            c.a = this->data[i][j];
            color_rgba tmp = color_split(surface_get_pixel(target, r.x + j, r.y + i), c);
            surface_set_pixel(target, tmp, j + r.x, i);
        }
    }
}
