#include "../include/cmds.h"

#include "../include/ish.h"
#include "../include/stdio.h"
#include "../include/arch/i686/io.h"
#include "../include/arch/i686/kbd.h"
#include "../include/arch/i686/smbios.h"
#include "../include/tty_old.h"
#include "../include/string.h"
#include "../include/errno.h"
#include "../include/arch/i686/rtc.h"
#include "../include/panic.h"
#include "../include/surface.h"
#include "../include/bmp.h"
#include "../include/font.h"

void emptyCmd(){}

void help(int argc, char **argv)
{
    int argc_orig=argc;
    if(argc>1)
    {
        while(argc-->=1)
        {
            int i;
            for(i=0; i<=256 && strcmp(cmds[i].name, argv[argc_orig-argc])!=0; i++)
                ;
            printf("%s\t\t%s\n", cmds[i].name, cmds[i].description);
        }
        return;
    }
    printf(
        "ISH 1.0\n"
        "U365 Kernel Integrated Shell. Known commands:\n");

    int maxlen = 0;
    int currlen;
    int i;

    for(i = 0; i < cmdNum; ++i)
    {
        currlen = strlen(cmds[i].name);
        if(maxlen < currlen)
        {
            maxlen = currlen;
        }
    }
    for(i = 0; i < cmdNum; ++i)
    {
        printf("[%-*s]    %s\n", maxlen, cmds[i].name, cmds[i].description);
    }
}

void sysinfo(int argc,char** argv)
{
    int argc_orig=argc;
    if(argc==1)
        printf(
            "U365 system info\n"
            "CPU Frequency       %d Mhz\n"
            "BIOS Vendor         %s\n"
            "BIOS Version        %s\n"
            "BIOS Release Date   %s\n",
            getCPUFreq(),
            (smbios_detected) ? detectBIOSVendor() : "ERR: No SMBIOS",
            (smbios_detected) ? detectBIOSVersion() : "ERR: No SMBIOS",
            (smbios_detected) ? detectBIOSBDate() : "ERR: No SMBIOS");
    else
    while(argc--)
    {
        if(!strcmp(argv[argc_orig-argc],"--help"))
            printf("U365 Kernel\n"
                   "--help\t\tDisplay this message\n"
                   "freq\t\tDisplay CPU Frequency\n"
                   "bvndr\t\tDisplay BIOS vendor\n"
                   "bver\t\tDisplay BIOS version\n"
                   "brldt\t\tDisplay BIOS release date\n"
                   "<no args>\tDisplay all system data.");
        if(!strcmp(argv[argc_orig-argc], "freq"))
                printf("CPU Frequency       %d Mhz\n",getCPUFreq());
        if(!strcmp(argv[argc_orig-argc], "bvndr"))
                printf("BIOS Vendor         %s\n",(smbios_detected) ? detectBIOSVendor() : "ERR: No SMBIOS");
        if(!strcmp(argv[argc_orig-argc], "bver"))
                printf("BIOS Version        %s\n",(smbios_detected) ? detectBIOSVersion() : "ERR: No SMBIOS");
        if(!strcmp(argv[argc_orig-argc], "brldt"))
                printf("BIOS Release Date   %s\n",(smbios_detected) ? detectBIOSBDate() : "ERR: No SMBIOS");
    }

}

void atable()
{
    for(int i=0; i<128; i++)
        tty_putchar(i);
}
void smb_data()
{
    char *sm=(char*)smb_ep;
    for(int i=0; i<1024; i++)
        tty_putchar(*sm++);
}

void kotiki()
{
    initTTY();
    printf("KOTIKI KOTIKI KOOOTEEEKEEEYYY\n");
    printf("/\\_/\\ ♥\n\
<^,^>\n\
/ \\\n\
(__)__\n\
");
    printf("___________________________$$$$$________$$$$______\n\
___________________________$_$$$$$$$$$$$$$$$______\n\
___________________________$__$$$______$$$_$______\n\
____________________________$_____________$$______\n\
___________________________$$____________$$_______\n\
___________________________$$__$$$$____$__$$______\n\
_______________________$$$$$$$$_$$$___$$$_$_______\n\
_______________________$$$$$$$$$$_________$_______\n\
____________________________$$$_$$_$$$_$$$$$$$$___\n\
__________________________$$$$$$$__$$$$$$$$$$__$__\n\
________________________$$$$$$_$$$$$$$$$$$___$____\n\
_______________________$$__$$_____$$$___$$_$______\n\
$$$$$$________________$$$___$__________$$$__$_____\n\
$$$__$$______________$$_____$$__________$$________\n\
__$$__$$____________$$______$$_________$$$________\n\
___$$__$$___________$$_______$$______$$$$$________\n\
____$__$$__________$$________$$$$___$$__$$________\n\
____$$__$$_______$$$$_______________$___$$________\n\
_____$$__$______$$______________________$$________\n\
_____$$__$$____$$_______________________$$________\n\
______$__$$___$$_______$$$$___$$___$$___$_________\n\
______$$__$$__$$________$$$___$$__$$___$$_________\n\
_______$__$$__$$__________$$___$$_$___$$__________\n\
_______$$__$$_$$__________$$___$$$$___$$__________\n\
________$$__$$$$___________$____$$$___$___________\n\
_________$$_$$$$$__________$$___$$$___$___________\n\
__________$$__$$$_________$$$$__$$$$__$$__________\n\
____________$$$$$_________$$$$___$$$___$$$________\n\
______________$$$$$____$_$_$$$$_$_$$__$_$$________\n\
__________________$$$$$$$$$$$$$$$$$$$$$$$_________\n\
_______________$$$$$$$$$__________________________\n");
    panic("CATS HAVE TAKEN OVER THE CONTROL!!!");
}

void gpf()
{
    asm("int $99");
}

void cmd_cat(int argc, char** argv)
{
    if(argc==1)
    {
        perr("cat: filename(s) expected");
        return;
    }
    if(argc>=64)
    {
        perr("cat: too many input files");
        return;
    }
    FILE* fp;
    int argc_orig=argc;
    errno=0; //TODO: it needs to be nulled by libc
    while(argc-->1)
    {
        fp = fopen(argv[argc_orig-argc], "r");
        if(!errno)
        {
            char c;
            while((c=fgetc(fp))!=EOF)
                tty_putchar(c);
            tty_putchar('\n');
            fclose(fp);
        }
        else
        {
            printf("Failed to open file: %s: %s\n", argv[argc_orig-argc], strerror(errno));
        }
    }
}

void cmd_echo(int argc, char** argv)
{
    int argc_orig=argc;
    while(argc-- > 1)
    {
        printf("%s",argv[argc_orig-argc]);
        tty_putchar(' ');
    }
    tty_putchar('\n');
}

void cmd_vfs_test()
{
    errno=0; //TODO: it needs to be nulled by libc
    printf("Calling fopen...\n");
    FILE* test_vfs = fopen("/logo.bmp", "r");
    if(errno!=0)
    {
        perror("testvfs: can't open file");
        return;
    }
    printf("done. Starting read process...\n\n");
    int16_t c;
    while((c=fgetc(test_vfs))!=EOF)
        if(c<128)
        tty_putchar(c);
    else
        printf("0x%02X ", c);
    printf("\nSee result in terminal\n");
}

void cmd_errno_test()
{
    printf("errno test. You'll see the error - don't panic! It's manually generated by this code.\n");
    printf("We'll open non-existent file \"/not_existent_file.tmp\", so fopen(2) will set errno to ENOENT.\n");
    fopen("/not_existent_file.tmp", "r"); //We doesn't have file with that name, so that code will error.
    perror("Error text");
    printf("errno value: %d\n", errno);
    errno=0;
}
void cmd_all_test()
{
    printf("Welcome to U365 full system test.\n");
    printf("BMP display test.\n");
    showlogo();
    printf("sleep(1) test. ");
    sleep(1000);
    printf("1000 ms passed.\n");
    printf("VFS test.\n");
    cmd_vfs_test();
    printf("\nErrno test.\n");
    cmd_errno_test();
    printf("\nDate/time test.\n");
    cmd_date();
}

void showlogo()
{
    FILE* lf = fopen("/logo.bmp", "r");
    if(lf)
    {
        surface* ls = bmp_read(lf);
        if(ls)
        {
            rect lr =
            {
                (surface_screen->w - ls->w) / 2,
                (surface_screen->h - ls->h) / 2,
                (surface_screen->w + ls->w) / 2,
                (surface_screen->h + ls->h) / 2,
            };
            surface_apply(surface_screen, ls, lr);
            surface_delete(ls);
            return;
        }
    }
    printf("Failed to read logo\n");
    //fclose(lf);
}
void cmd_reboot()
{
    fprintf(stderr, "\nSystem is rebooting in 5 seconds.\n");
    sleep(4150);
    fprintf(stderr, "THE SYSTEM IS GOING TO REBOOT NOW!\n");
    sleep(850);
    //Only kbd reboot is supported at this time.
    kbd_reset_cpu();
}
void cmd_date()
{
    read_rtc();
    printf("Current UTC date/time is:\n"
           "%02d:%02d:%02d\n"
           "%02d/%02d/%04d\n"
           "Current UNIX(tm) timestamp is %u.\n",hour,minute,second,day,month,year,get_current_timestamp());
}
void cmd_lscpu()
{
    detect_cpu();
}

void bfntest()
{
    surface *sf = bfn_write_str("BFN test. Let's see some characters. ~!@#$%^&*()_+|`1234567890-=\\{}\"'/`\n\0", color_yellow);
    rect r={50,500,sf->w,sf->h};
    surface_apply(surface_screen,sf,r);
}
void surface_test()
{
    /*rect r2 = {50,50,500,25};
    color_rgba c2 ={0xC5, 0xE1, 0xA5, 0xFF };
    surface_fill_rect(surface_screen, c2, r2);
    surface* sf=bfn_write_str("Surface test. 12345", color_black);
    rect r3 = {50+5,50+5,sf->w,sf->h};
    surface_apply(surface_screen, sf, r3);*/
    rect background_rect = {0,0,surface_screen->w, surface_screen->h};
    rect panel_rect = {0, surface_screen->h-40, surface_screen->w, 40};
    rect wnd_inner_rect = {200, 200, 700, 400};
    rect wnd_caption_rect = {200, 200, 700, 40};
    rect wnd_shadow_rect = {202, 202, 700, 400};

    color_rgba background_color ={0xAA, 0xAA, 0xAA, 0xAA};
    color_rgba panel_color ={0xDD, 0xDD, 0xDD, 0xFF};
    color_rgba wnd_inner_color ={0xFF, 0xCC, 0x00, 0xFF};
    color_rgba wnd_caption_color ={0xFF, 0xFF, 0xFF, 0xFF};
    color_rgba wnd_shadow_color ={0x44, 0x44, 0x44, 0xFF};
    surface_fill_rect(surface_screen, background_color, background_rect);
    surface_fill_rect(surface_screen, panel_color, panel_rect);
    surface *sf=surface_new();
    surface_fill_rect(sf, wnd_shadow_color, wnd_shadow_rect);
    surface_fill_rect(sf, wnd_inner_color, wnd_inner_rect);
    surface_fill_rect(sf, wnd_caption_color, wnd_caption_rect);
    surface_apply(surface_screen, sf, background_rect);
    while(1);
}
void mouse_test()
{
    printf("Mouse test. You'll see mouse coordinates on the screen. To finish the test, press left mouse button.\n");
    FILE *mouse_data=fopen("/dev/mouse", "r");
    uint32_t *buf=(uint32_t*) malloc(5*4);
    fread(buf, 4, 5, mouse_data);
    printf("\rX: %04d Y: %04d", mouse_data[2], mouse_data[3]);
    fseek(mouse_data, 0, SEEK_SET);
    fread(buf, 4, 5, mouse_data);
    printf("\nX: %04d Y: %04d", mouse_data[2], mouse_data[3]);
}