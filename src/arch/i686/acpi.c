#include "../../../include/arch/i686/acpi.h"

#include "../../../include/stdio.h"
#include "../../../include/time.h"
#include "../../../include/debug.h"
#include "../../../include/arch/i686/io.h"

struct FADT      *fadt    ; //Fixed ACPI Description Table.
struct DSDT_vals  dsdt    ; //Distributed System Description Table.
void             *dsdt_ptr;

void *findRSDP()
{
	debug_print(NOTICE,"ACPI: Finding RSDP...");
	uint8_t *mem=(uint8_t*) 0x000E0000;
	while((unsigned int)mem!=0x000FFFFF)
	{
		if(memcmp(mem, "RSD PTR ", 8)==0)
		{
			debug_print(NOTICE,"ACPI: RSDP signature valid. Validating checksum...");
			uint8_t *bptr = (uint8_t *) mem;
			uint8_t check=0;
      		for (unsigned int i=0; i<sizeof(struct RSDPDescriptor); i++)
      		{
         		check += *bptr;
         		bptr++;
      		}
      		if(((struct RSDPDescriptor*) mem)->Revision>=1)
      		{
      			check=0;
      			uint8_t *bptr20 = (uint8_t *) mem;
      			for (unsigned int j=0; j<sizeof(struct RSDPDescriptor20); j++)
      			{
         			check += *bptr20;
         			bptr20++;
      			}
      		}
      		if(!check)
      		{
      			debug_print(SUCCESS,"ACPI: Checksum is valid. ACPI is present on this system.");
      			printf("ACPI Version: %d\n", ((struct RSDPDescriptor*) mem)->Revision+1);
				return mem;
			}
			else
			{
				fatal_error(EACPI_RSDP_CHKSM_INVALID);
			}
		}
		mem++;
	}
	debug_print(ERROR, "ACPI: RSDP table not found. ACPI won't work.");
	return 0;
}
int acpi_check_hdr(uint8_t *table, const char *sig, int n)
{
	if(memcmp(table,sig,n)!=0)
		return 1;
	uint8_t checksum=0;
	//for(unsigned int i=0; i<sizeof(struct ACPISDTHeader); i++)
	//{
	//	checksum+=*table++;
	//}
	if(checksum==0)
		return 0;
	return 1;
}
void acpi_enable()
{
	debug_print(NOTICE,"RSDP, RSDT/XSDT and FADT was found, DSDT was parsed. Trying to enable ACPI...");
	if((inw(fadt->PM1aControlBlock) &1) == 0 )
	{
		debug_print(SUCCESS,"ACPI was already enabled, maybe with BIOS.");
		return;
	}
	outb(fadt->SMI_CommandPort,fadt->AcpiEnable);
	int i;
	for( i=0; i<300; i++)
	{
		if((inw(fadt->PM1aControlBlock) &1) == 0)
			break;
		sleep(10);
	}
	if((inw(fadt->PM1aControlBlock) &1) != 0)
	{
		fatal_error(EACPI_ENABLE_ERROR);
	}
	debug_print(SUCCESS,"ACPI was successfully enabled!");
}
void parse_dsdt()
{
	debug_print(NOTICE,"ACPI: Parsing DSDT");
	if(fadt==0)
	{
		fatal_error(EACPI_DSDT_NOFADT);
	}
	if(acpi_check_hdr((uint8_t*)(int)fadt->Dsdt,"DSDT",4)!=0)
	{
		fatal_error(EACPI_DSDT_INVALID);
	}
	debug_print(NOTICE,"ACPI: DSDT signature valid. Starting parse process...");
	dsdt_ptr=(void*)fadt->Dsdt;
	dsdt.length=((struct ACPISDTHeader *) dsdt_ptr)->Length-sizeof(struct ACPISDTHeader);
	//Find the S5 object.
	int i;
	for(i=0; i<=dsdt.length; i++)
	{
		if(memcmp(dsdt_ptr+sizeof(struct ACPISDTHeader)+i,"_S5_",4)==0)
		{
			dsdt.S5_object=dsdt_ptr+i;
			break;
		}
	}
	if(i==dsdt.length)
	{
		fatal_error(EACPI_DSDT_NOS5);
	}
}
void parse_acpi(void *mem)
{
	//First set up the tables.
	struct RSDPDescriptor *rsdp=mem;
	struct RSDT           *rsdt=(struct RSDT*)(rsdp->RsdtAddress);
	int i=0;
	//Then check the headers and print an errors if neccesary
	if(acpi_check_hdr((uint8_t*)(int)rsdt,"RSDT",4)!=0)
	{
		fatal_error(EACPI_RSDT_INVALID);
	}
	while(acpi_check_hdr((uint8_t*)rsdt->PointerToOtherSDT[i],"FACP",4)!=0 && i<=64)
		i++;
	if(i<64)
	{
		fadt=(struct FADT*)rsdt->PointerToOtherSDT[i];
	}
	else
	{
		fatal_error(EACPI_FADT_NOT_FOUND);
	}
	//Then parse the DSDT and enable ACPI.
	parse_dsdt();
	acpi_enable();
}
void parse_acpi20(void *mem)
{
	//First set up the tables.
	struct RSDPDescriptor20 *rsdp=(struct RSDPDescriptor20*)mem;
	struct XSDT           *xsdt=(struct XSDT*)(int)(rsdp->XsdtAddress);
	int i=0;
	//Then check the headers and print an errors if neccesary
	while(acpi_check_hdr((uint8_t*)(int)xsdt->PointerToOtherSDT[i],"FACP",4)!=0 && i<=64)
		i++;
	if(i<64)
	{
		fadt=(struct FADT*)(int)xsdt->PointerToOtherSDT[i];
	}
	else
	{
		fatal_error(EACPI_FADT_NOT_FOUND);
	}
	//Then parse the DSDT and enable ACPI.
	parse_dsdt();
	acpi_enable();
}
void init_acpi()
{
	struct RSDPDescriptor *rsdp=findRSDP();
	if(rsdp==0)
	{
		tty_putchar('\n');
		debug_print(FATAL,"No ACPI found. Your PC isn't compatible (are you tried to start U365 on non-IBM PC-compatible computer?)");
		fatal_error(ENOTCOMPAT);
	}
	if(rsdp->Revision>=1)
	{
		parse_acpi20((void*)rsdp);
		return;
	}
	parse_acpi((void*)rsdp);
}
