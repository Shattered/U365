; Syscall wrapper for calling C syscall function.
; Very small; only calls a C function.
extern syscall
global syscall_wrapper
syscall_wrapper:
; Give the registers for C handler
call syscall
iretd