#include "../../../include/arch/i686/kbd.h"

unsigned char scancode[256] = {
    '\0', '\e', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '-', '=', '\b', '\t',
    'q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p', '[', ']',
    '\n', 128+12+1 /* Left Ctrl */,
    'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', ';', '\'',
    '`', 128+12+2 /* Left Shift */, '\\', 'z', 'x', 'c', 'v', 'b', 'n', 'm', ',', '.', '/',
    128+12+3 /* Right Shift */, '*', 128+12+4 /* Left Alt */, ' ', 128+12+5 /* Caps */, 
    //F# keys.
    128+1, 128+2, 128+3, 128+4, 128+5, 128+6, 128+7, 128+8, 128+9, 128+10,  
    128+12+6 /* NumLk */, 128+12+7 /* ScrollLk */, '7', '8', '9', '-', '4', '5', '6', '+', '1', '2', '3', '0', '.',
    0, 0, 0, 0, /* they're never used. So they're zero. */
    //F11 & F12
    128+11, 128+12, 
};


/* Keyboard functions. */
//Internal function used by getchar. Reads 1 scancode from keyboard buffer.
uint8_t kbd_getScancode()
{
    kbd_wait_irq();
    //kbd_wait_irq();
    return inb(KBD_DATA);
}
uint8_t kbd_getchar()
{
    return kbd_scanToChr(kbd_getScancode());

}
size_t kbd_getsn(char* s, size_t num)
{
    char *cmd=malloc(128);
    uint8_t c=0;
    bool caps=false, shift=false;
    size_t i=0;
    while(true)
    {
        c=kbd_getchar();
        if(c>0)
        {
           if(c=='\n' || i>num)
           {
               cmd[i]='\0';
               strcpy(s,cmd);
               return i;
           }
           if(c==128+12+5)
           {
               caps=!caps;
               continue;
           }
           if(c==128+12+2 || c==128+12+3)
           {
               shift=true;
               continue;
           }
           if(c==128+12+8 || c==128+12+9)
           {
               shift=false;
               continue;
           }
           if(c=='\b')
           {
              if(i>0)
              {
                tty_x--;
                putchar(' ');
                tty_x--;
                cmd[i--]=0;
              }
           }
           if((c>='a' && c<='z') || (c>=' ' && c<='@'))
           {
               if(!caps && !shift)
               {
                   putchar(c);
                   cmd[i++]=c;
               }
               else
               {
                  if(c>='a' && c<='z' )
                  {
                    putchar(c-32);
                    cmd[i++]=c-32;
                  }
                  else if(c>='1' && c<='6' )
                  {
                    putchar(c-7);
                    cmd[i++]=c-7;
                  }
                  else if(c>='6' && c<='9' )
                  {
                    putchar(c-8);
                    cmd[i++]=c-8;
                  }
               }

            
           }
        }
    }
}
size_t kbd_gets(char *s)
{
    uint8_t c=0;
    bool caps=false, shift=false;
    int i=0;
    while(true)
    {
        c=kbd_getchar();
        if(c>0)
        {
           if(c=='\n')
           {
               s[i]='\0';
               return i;
           }
           if(c==128+12+5)
           {
               caps=!caps;
               continue;
           }
           if(c==128+12+2 || c==128+12+3)
           {
               shift=true;
               continue;
           }
           if(c==128+12+8 || c==128+12+9)
           {
               shift=false;
               continue;
           }
           if(c=='\b')
           {
              if(i>0)
              {
                tty_x--;
                tty_putchar(' ');
                tty_x--;
                s[i--]=0;
              }
           }
           if((c>='a' && c<='z') || (c>=' ' && c<='@') || c=='-' || c=='=' || c=='\\')
           {
               if(!caps && !shift)
               {
                   putchar(c);
                   s[i++]=c;
               }
               else
               {
                  if(c>='a' && c<='z' )
                  {
                    putchar(c-32);
                    s[i++]=c-32;
                  }
                  else if(c>='1' && c<='5' )
                  {
                    putchar(c-16);
                    s[i++]=c-16;
                  }
                  else if(c>='6')
                  {
                    putchar(c-17);
                    s[i++]=c-17;
                  }
               }

            
           }
        }
    }
}
/*
This function is waits for keyboard buffer to empty.
*/
void kbd_waitForBufToEmpty()
{
    char c=inb(0x60);
    while(inb(0x60)==c)
        c=inb(0x60);
}
void kbd_scancodes_setup()
{
  scancode[0xaa]=128+12+8;
  scancode[0xb6]=128+12+9;
}
//Keyboard-powered CPU reset.
void kbd_reset_cpu()
{
    asm("cli");
    uint8_t good = 0x02;
    while (good & 0x02)
        good = inb(0x64);
    outb(0x64, 0xFE);
    tty_wrstr("Keyboard CPU reset failed.");
    asm("hlt");
}
