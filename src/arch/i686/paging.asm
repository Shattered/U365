global load_pg_dir
load_pg_dir:
	push ebp
	mov ebp, esp
	mov eax, [esp+8]
	mov cr3, eax
	mov esp, ebp
	pop ebp
	ret
global paging_enable
paging_enable:
	push ebp
	mov ebp, esp
	mov eax, cr0
	or eax, 0x80000000 ; Enable the paging bit
	mov cr0, eax       ; Fire up the paging!!!
	extern memcpy
	push 480000
	push 0
	push 0fd000000h
	call memcpy
	cli                ; We need to halt here,
	hlt                ; so it'll don't access
					   ; our non-mapped framebuffer.
	mov esp, ebp
	pop ebp
	ret