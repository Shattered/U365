#include "../../../include/arch/i686/devices.h"

#include <stdint.h>
#include <stdbool.h>

#include "../../../include/arch/i686/io.h"
#include "../../../include/arch/i686/idt.h"
#include "../../../include/tty_old.h"
#include "../../../include/sys.h"
#include "../../../include/vfs.h"
#include "../../../include/debug.h"
#include "../../../include/drawing.h"
#include "../../../include/stdio.h"
#include "../../../include/surface.h"

//Thanks for Brandon F. for PIT code and Santik for mouse code.
struct SystemRTInfo srt_info;
void init_srtInfo()
{
    srt_info.devices_num=0;
}
void timer_phase(int hz)
{
    int divisor = 1193180 / hz;       /* Calculate our divisor */
    outb(0x43, 0x36);             /* Set our command byte 0x36 */
    outb(0x40, divisor & 0xFF);   /* Set low byte of divisor */
    outb(0x40, divisor >> 8);     /* Set high byte of divisor */
}
bool _sleepCurrently=false;
unsigned long msLeft;
volatile unsigned long long int timer_ticks;
bool cursor_displayed=false;
void timer_handler(regs* UNUSED(r))
{
    timer_ticks++;
    if(_sleepCurrently)
    {
        msLeft--;
    }
    if(!msLeft) _sleepCurrently=false;

}

void sleep(int ticks)
{
    unsigned int eticks;

    eticks = timer_ticks + ticks;
    while(timer_ticks < eticks)
    {
        __asm__ __volatile__ ("sti//hlt//cli");
    }
}

void setupPIT()
{
    timer_phase(1000);
    irq_install_handler(0, timer_handler);
}

unsigned char mouse_cycle=0;
  signed char mouse_byte[3];

int invert(int n, int max)
{
    return max-n;
}
uint32_t cursor[257] = {0xFFFFFF,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,
    0xFFFFFF,0xFFFFFF,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,
    0xFFFFFF,0x000001,0xFFFFFF,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,
    0xFFFFFF,0x000001,0x000001,0xFFFFFF,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,
    0xFFFFFF,0x000001,0x000001,0x000001,0xFFFFFF,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,
    0xFFFFFF,0x000001,0x000001,0x000001,0x000001,0xFFFFFF,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,
    0xFFFFFF,0x000001,0x000001,0x000001,0x000001,0x000001,0xFFFFFF,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,
    0xFFFFFF,0x000001,0x000001,0x000001,0x000001,0x000001,0x000001,0xFFFFFF,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,
    0xFFFFFF,0x000001,0x000001,0x000001,0x000001,0x000001,0x000001,0x000001,0xFFFFFF,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,
    0xFFFFFF,0x000001,0x000001,0x000001,0x000001,0x000001,0x000001,0x000001,0x000001,0xFFFFFF,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,
    0xFFFFFF,0x000001,0x000001,0x000001,0x000001,0x000001,0x000001,0xFFFFFF,0xFFFFFF,0xFFFFFF,0xFFFFFF,0x000000,0x000000,0x000000,0x000000,0x000000,
    0xFFFFFF,0x000001,0x000001,0x000001,0x000001,0x000001,0xFFFFFF,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,
    0xFFFFFF,0x000001,0x000001,0x000001,0x000001,0x000001,0xFFFFFF,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,
    0xFFFFFF,0x000001,0xFFFFFF,0xFFFFFF,0xFFFFFF,0x000001,0x000001,0xFFFFFF,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,
    0xFFFFFF,0xFFFFFF,0x000000,0x000000,0xFFFFFF,0x000001,0x000001,0xFFFFFF,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,
    0x000000,0x000000,0x000000,0x000000,0x000000,0xFFFFFF,0xFFFFFF,0xFFFFFF,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000,0x000000};
uint32_t mx;
uint32_t my;
//Mouse functions

color_rgba ums[18][18];

void mouse_handler(regs* UNUSED(a_r)) //struct regs *a_r (not used but just there)
{
    int8_t ox = 0;
    int8_t oy = 0;

    switch(mouse_cycle)
    {
        case 0:
            mouse_byte[0]=inb(0x60);
            mouse_cycle++;
            break;
        case 1:
            mouse_byte[1]=inb(0x60);
            mouse_cycle++;
            break;
        case 2:
            mouse_byte[2]=inb(0x60);
            ox=mouse_byte[1];
            oy=mouse_byte[2];
            mouse_cycle=0;
            break;
    }

    int32_t tx = mx;
    int32_t ty = my;

    tx += ox;
    ty -= oy;

    for(int i = 0; i < 18; ++i)
    {
        for(int j = 0; j < 18; ++j)
        {
            rect r = {j * 4, i * 4, 4, 4};
            surface_fill_rect(surface_screen, ums[i][j], r);
        }
    }
    for(int i = 0; i < 18; ++i)
    {
        for(int j = 0; j < 18; ++j)
        {
            surface_set_pixel(surface_screen, ums[i][j], mx + j - 1, my + i - 1);
        }
    }

    //surface_apply(surface_screen, ums, umr);

    if(tx < 0)
        tx = 0;
    if(tx >= (int32_t)(surface_screen->w))
        tx = surface_screen->w;
    if(ty < 0)
        ty = 0;
    if(ty >= (int32_t)(surface_screen->h))
        ty = surface_screen->h;
    mx = tx;
    my = ty;
    for(int i = 0; i < 18; i++)
    {
        for(int j = 0; j < 18; j++)
        {
            ums[i][j] = surface_get_pixel(surface_screen, mx + j - 1, my + i - 1);
        }
    }
    // Redraw the cursor
    drawBitmap(tx, ty, cursor,16,16);
    if((mouse_byte[0] >> 3 & 1)==0)
    {
        rect r={tx-2, ty-2, 4, 4};
        color_rgba c={0xFF, 0xCC, 0x00, 0xFF};
        surface_fill_rect(surface_screen, c, r);
    }
}

void mouse_wait(uint8_t a_type) //unsigned char
{
    uint32_t _time_out=100000; //unsigned int
    if(a_type==0)
    {
        while(_time_out--) //Data
        {
            if((inb(0x64) & 1)==1)
            {
                return;
            }
        }
        return;
    }
    else
    {
        while(_time_out--) //Signal
        {
            if((inb(0x64) & 2)==0)
            {
                return;
            }
        }
        return;
    }
}

void mouse_write(uint8_t a_write) //unsigned char
{
    //Wait to be able to send a command
    mouse_wait(1);
    //Tell the mouse we are sending a command
    outb(0x64, 0xD4);
    //Wait for the final part
    mouse_wait(1);
    //Finally write
    outb(0x60, a_write);
}

uint8_t mouse_read()
{
    //Get's response from mouse
    mouse_wait(0);
    return inb(0x60);
}

void mouse_install()
{
    /*for(int i=0; i<8; i++)
        for(int j=0; j<5; j++)
            pixels[i][j]=getPix(mx+j,my+i);*/
    uint8_t _status;  //unsigned char
    //Enable the auxiliary mouse device
    mouse_wait(1);
    outb(0x64, 0xA8);
    //Enable the interrupts
    mouse_wait(1);
    outb(0x64, 0x20);
    mouse_wait(0);
    _status=(inb(0x60) | 2);
    mouse_wait(1);
    outb(0x64, 0x60);
    mouse_wait(1);
    outb(0x60, _status);
    //Tell the mouse to use default settings
    mouse_write(0xF6);
    mouse_read();  //Acknowledge
    //Enable the mouse
    mouse_write(0xF4);
    mouse_read();  //Acknowledge
    //Setup the mouse handler

    irq_install_handler(12, mouse_handler);

    mx = surface_screen->w / 2;
    my = surface_screen->h / 2;
}
volatile bool kbd_irq_fired=false;
void kbd_wait_irq()
{
    //if(kbd_irq_fired){while(kbd_irq_fired);return;}
    while(!kbd_irq_fired);
    kbd_irq_fired=false;
}
void kbd_handler(struct regs *UNUSED(r))
{
    kbd_irq_fired=true;
}
void initKbdInt()
{
    irq_install_handler(1,kbd_handler);
}

