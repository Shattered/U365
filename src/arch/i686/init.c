#include "../../../include/stdio.h"
#include "../../../include/arch/i686/idt.h"
#include "../../../include/arch/i686/gdt.h"
#include "../../../include/arch/i686/io.h"
#include "../../../include/sys.h"
#include "../../../include/debug.h"
#include "../../../include/arch/i686/devices.h"
#include "../../../include/arch/i686/acpi.h"
#include "../../../include/arch/i686/kbd.h"
#include "../../../include/arch/i686/asmdefs.h"
#include "../../../include/arch/i686/smbios.h"
#include "../../../include/arch/i686/paging.h"

/**
 * This file is part of U365 kernel.
 * DESC
 * Initializing of arch-specific features.
 */

void arch_init() //Initializing of x86-specific features.
{
    debug_print(INIT,"Masking IRQ vectors");
    outb(0x21,0xfd);
    outb(0xa1,0xff);
    debug_print(INIT,"Setting up PIC... ");
    init_pics(0x20,0x28);
    debug_print(INIT,"Setting up GDT... ");
    gdt_install();
    debug_print(INIT,"Setting up IDT... ");
    idt_install();
    debug_print(INIT,"Setting up ISRs...");
    isrs_install();
    debug_print(INIT,"Setting up IRQs...");
    irq_install();
    debug_print(INIT,"Setting up PIT... ");
    setupPIT();
    debug_print(INIT,"Setting up keyboard driver... ");
    initKbdInt();
    kbd_scancodes_setup();
    debug_print(INIT,"Detecting SMBIOS...");
    unsigned char *mem = (unsigned char *) 0xF0000;
    int length, i;
    unsigned char checksum;
    while ((unsigned int) mem < 0x100000)
    {
        if (memcmp(mem,"_SM_",4)==0)
        {
            debug_print(INIT,"_SM_ pattern found, validating checksum");
            length = mem[5];
            checksum = 0;
            for(i = 0; i < length; i++)
            {
                checksum += mem[i];
            }
            debug_print(INIT,"Done");
            if(checksum == 0) break;
            debug_print(INIT,"Invalid checksum");
        }
        mem += 16;
    }
    debug_print(INIT,"Cycle ended");
    //serial_out("sm\n");
    if ((unsigned int) mem == 0x100000)
    {
        debug_print(WARNING,"No SMBIOS found.");
        //serial_out("smerr\n");
    }
    smb_ep = (struct SMBIOSEntryPoint*)mem;
    debug_print(INIT,"Gathering hardware information");
    machine_bios_vendor=  detectBIOSVendor();
    machine_bios_version=detectBIOSVersion();
    machine_bios_bdate=    detectBIOSBDate();
    //serial_out("cpu speed detected\n");
    if(memcmp(mem,"_SM_",4)==0)
    {
        debug_print(INIT,"SMBIOS detected.");
        smbios_detected=true;
    }
    else
    {
        debug_print(WARNING,"SMBIOS entry point not found. OS functions which are displaying PC hardware info won't work.");
    }
    //outb(0x21,0xfc);
    //outb(0xa1,0x00);
    //setup_paging();
    for(int i = 0; i < 16; ++i)
    {
        IRQ_clear_mask(i);
    }
    INT_ON;
    printf("Detecting ACPI.\n");
    init_acpi();
}
