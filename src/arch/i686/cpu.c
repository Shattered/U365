#include "../../../include/arch/i686/io.h"
#include "../../../include/time.h"
#include "../../../include/sys.h"

uint16_t cpuMHZ;

uint16_t getCPUFreq()
{
    if(__u365_init_done)
        return cpuMHZ;
    uint64_t tsc=rdtsc();
    sleep(1000);
    uint64_t tsc2=rdtsc();
    return (tsc-tsc2)/1000/1000;
}
