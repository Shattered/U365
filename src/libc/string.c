#include <string.h>

#include <stdint.h>
#include <stdbool.h>

size_t strlen(const char* str)
{
    size_t result = 0;
    while(str[result] != 0)
    {
        result++;
    }
    return result;
}

int atoi(const char* string)
{
    int self = 0;
    int mult = 10;
    bool negative = false;
    int values[strlen(string)];
    size_t count = 0;
    for(size_t id = 0; id < strlen(string); id++ )
    {
        if( string[id] == '-' ) negative = true;
        for( char ch = '0'; ch <= '9'; ch++ )
            if( string[id] == ch )
                values[count++] = (int)( (int)ch - (int)'0' );
    };
    self = values[count - 1];
    for(unsigned int id = 0; id < count - 1; id++ )
    {
        mult = mult * 10;
    }
    for( unsigned int id = 0; id < count - 1; id++ )
    { 
        mult = mult / 10;
        self = self + ( values[id] * mult );
    }
    if(negative) 
        return -self;
    else
        return self;
}

char* strrev(char* s)
{
    unsigned int i, j;
    char c;
    for(i = 0, j = strlen(s)-1; i<j; i++, j--)
    {
        c = s[i];
        s[i] = s[j];
        s[j] = c;
    }
    return s;
}

char *strcpy(char *s, const char *t)
{
    while((*s++ = *t++)); //Double brackets to remove the warning - do not remove.
    return s;
}

char *strcat(char *s, const char *t)
{
    return strcpy(s+strlen(s),t);
}

int strcmp(const char *s1, const char *s2)
{
    for ( ; *s1 == *s2; s1++, s2++)
        if (*s1 == '\0')
            return 0;
    return ((*(unsigned char *)s1 < *(unsigned char *)s2) ? -1 : +1);
}

char* strpbrk (const char *s, const char *accept)
{
    while (*s != '\0')
    {
        const char *a = accept;
        while (*a != '\0')
        if (*a++ == *s)
            return (char *) s;
        ++s;
    }
    return NULL;
}

size_t strspn (const char *s, const char *accept)
{
    const char *p;
    const char *a;
    size_t count = 0;

    for (p = s; *p != '\0'; ++p)
    {
        for (a = accept; *a != '\0'; ++a)
            if (*p == *a)
                break;
        if (*a == '\0')
            return count;
        else
            ++count;
    }
    return count;
}

char* strtok(char *s, const char *delim)
{
    static char* olds = NULL;
    char *token;

    if (s == NULL)
        s = olds;

    /* Scan leading delimiters.  */
    s += strspn (s, delim);
    if (*s == '\0')
    {
        olds = s;
        return NULL;
    }

    /* Find the end of the token.  */
    token = s;
    s = strpbrk (token, delim);
    if (s == NULL)
    {
        /* This token finishes the string.  */
        olds = token;
    }
    else
    {
        /* Terminate the token and make OLDS point past it.  */
        *s = '\0';
        olds = s + 1;
    }
    return token;
}
int memcmp(const void *s1, const void *s2, size_t n)                    /* Length to compare. */
{
    unsigned char u1, u2;

    for ( ; n-- ; s1++, s2++) {
    u1 = * (unsigned char *) s1;
    u2 = * (unsigned char *) s2;
    if ( u1 != u2) {
        return (u1-u2);
    }
    }
    return 0;
}
