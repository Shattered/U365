//U365 libc's errno support code.
int errno=0;
const char* errno_strs[256] = { //256 for future
	"", //Errno should NEVER be zero, except for OS boot, when it's initialized to 0.
	"No such file or directory",
	"No such device or address",
	"Read-only filesystem",
	"Not enough space",
	"Permission denied",
	"Operation not permitted",
};
const char* strerror(int err)
{
	return errno_strs[err];
}