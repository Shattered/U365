#include "../include/fio.h"

#include "../include/string.h"
#include "../include/memory.h"
#include "../include/tty_old.h"
#include "../include/stdio.h"
#include "../include/filesystem.h"

int16_t fgetc(FILE* stream)
{
    if(stream)
    {
        if(stream->getc)
        {
            return stream->getc(stream);
        }
        else
        {
            
            perr("fgetc: File is not opened for reading");
            return -1;
        }
    }
    return -1;
}

int16_t fputc(char ch, FILE* stream)
{
    if(stream)
    {
        if(stream->putc)
        {
            return stream->putc(stream, ch);
        }
        else
        {
            perr("File is not opened for writing");
            return -1;
        }
    }
    return -1;
}

size_t fgets(char* str, size_t num, FILE* stream)
{
    size_t result = 0;
    if(stream)
    {
        if(stream->gets)
        {
            result = stream->gets(stream, str, num);
        }
        else
        {
            if(stream->getc)
            {
                int16_t buf = 'b';
                while(result <= num && buf && buf != -1 && buf != '\n')
                {
                    buf = fgetc(stream);
                    if(buf != -1)
                    {
                        str[result++] = buf;
                    }
                }
                if(str[result - 1])
                {
                    str[result++] = 0;
                }
            }
            else
            {
                perr("File is not opened for reading");
            }
        }
    }
    return result;
}

size_t fputs(const char* str, FILE* stream)
{
    size_t result = 0;
    if(stream)
    {
        if(stream->puts)
        {
            result = stream->puts(stream, str);
        }
        else
        {
            if(stream->putc)
            {
                int16_t buf;
                while(*str)
                {
                    buf = fputc(str[result], stream);
                    if(buf != -1)
                    {
                        ++result;
                    }
                }
            }
            else
            {
                if(stream != stderr) // If this error will occur while writing to stderr, we will have an unexited recursion
                {
                    perr("File is not opened for writing");
                }
            }
        }
    }
    return result;
}

FILE* fopen(const char* path, const char* mode)
{
    FILE* f = calloc(sizeof(FILE), 1);
    freopen(path, mode, f);
    return f;
}

void freopen(const char* path, const char* mode, FILE* stream)
{
    fclose(stream);
    fs_open_file(root, path, mode, stream);
}

void fclose(FILE* stream)
{
    if(stream)
    {
        if(stream->close)
            stream->close(stream);
        free(stream->data);
        memset(stream, 0, sizeof(FILE));
    }
}

size_t fread(void* buf, size_t s, size_t n, FILE* f)
{
    size_t m = s * n;
    size_t i;
    for(i = 0; i < m; ++i)
    {
        int16_t r = fgetc(f);
        if(r == -1)
            break;
        ((char*)(buf))[i] = r;
    }
    return i / s;
}

int fprintf(FILE* stream, const char* fmt, ...)
{
    va_list va;
    va_start(va, fmt);
    int result = vfprintf(stream, fmt, va);
    va_end(va);
    return result;
}

int fseek(FILE* stream, long int offset, int origin)
{
    if(stream)
    {
        if(stream->seek)
            return stream->seek(stream, offset, origin);
    }
    return -1;
}

// Receiving functions that will do everything as needed

int64_t va_get_signed(int8_t mod, va_list* va)
{
    switch(mod)
    {
        case -2: return (signed char         )(va_arg(*va, int      )); break;
        case -1: return (signed short     int)(va_arg(*va, int      )); break;
        case  0: return (signed           int)(va_arg(*va, int      )); break;
        case  1: return (signed long      int)(va_arg(*va, long     )); break;
        case  2: return (signed long long int)(va_arg(*va, long long)); break;
        default: return 0;
    };
}

uint64_t va_get_unsigned(int8_t mod, va_list* va)
{
    switch(mod)
    {
        case -2: return (unsigned char         )(va_arg(*va, unsigned int      )); break;
        case -1: return (unsigned short     int)(va_arg(*va, unsigned int      )); break;
        case  0: return (unsigned           int)(va_arg(*va, unsigned int      )); break;
        case  1: return (unsigned long      int)(va_arg(*va, unsigned long     )); break;
        case  2: return (unsigned long long int)(va_arg(*va, unsigned long long)); break;
        default: return 0;
    };
}

/**
 * Formatting:
 * %[filler][space]
 * c: character
 * s: string
 * i: 10-base integer
 * d: 10-base integer
 */

int vfprintf(FILE* stream, const char* fmt, va_list va)
{
    size_t i;
    size_t n = strlen(fmt);
    size_t j;
    int64_t k;
    int result = 0;

    for(i = 0; i < n; i++)
    {
        if(fmt[i] == '%')
        {
            uint8_t  state      = 0; // will be set to 1 when data is ready and to 2 when data is put out
            uint8_t  flag_dot   = 0;
            uint8_t  flag_octo  = 0;
            uint8_t  flag_minus = 0;
            uint8_t  flag_plus  = 0;
            uint8_t  flag_space = 0;
            uint8_t  flag_zero  = 0;

            uint8_t  radix;
            uint8_t  uppercase = 0;

            size_t   minlen    = 0;
            size_t   precision = -1;

            uint8_t  sig       = 0;
            int8_t   modifiers = 0;
            uint64_t out       = 0; // all of the types can be placed into uint64_t, so we'll use it to temporarily store the value

            size_t bufsize = 64;
            uint8_t neg = 0;
//            char buf[64];

            for(j = i + 1; j < n && !state; ++j)
            {
                //memset(buf, 0, 64);
                switch(fmt[j])
                {
                    // Weird
                    case '%': fputc('%', stream); state = 2; break;
                    // Flags
                    case '.': flag_dot   = 1; break;
                    case '#': flag_octo  = 1; break;
                    case '-': flag_minus = 1; break;
                    case '+': flag_plus  = 1; break;
                    case ' ': flag_space = 1; break;
                    case '0': flag_zero  = 1; break;
                    case 'l': if(modifiers >= 0 && modifiers <  2) ++modifiers; break;
                    case 'h': if(modifiers <= 0 && modifiers > -2) --modifiers; break;
                    case '*':
                    {
                        if(flag_dot)
                        {
                            precision = va_arg(va, int);
                        }
                        else
                        {
                            minlen = va_arg(va, int);
                        }
                        break;
                    }

                    // Output types
                    case 'c':
                    {
                        // character, passed as int
                        fputc(va_arg(va, int), stream);
                        sig = 1;
                        state = 2;
                        break;
                    }
                    case 's':
                    {
                        const char* str = va_arg(va, const char*);
                        if(str)
                        {
                            size_t len = strlen(str);
                            size_t pref=0;
                            if(!flag_minus && len < minlen)
                            {
                                for(pref = 0; pref < minlen-len && pref < precision; ++pref)
                                {
                                    fputc(flag_zero ? '0' : ' ', stream);
                                }
                            }
                            for(k = 0; k < len && k + pref < precision; ++k)
                            {
                                fputc(str[k], stream);
                            }
                            pref = k;
                            if(flag_minus && len < minlen)
                            {
                                for(; k < minlen && k < precision; ++k)
                                {
                                    fputc(' ', stream);
                                }
                            }
                            result += k + pref;
                        }
                        state = 2;
                        break;
                    }
                    case 'd': case 'i':
                    {
                        // signed decimal
                        out   = va_get_signed(modifiers, &va);
                        sig   = 1;
                        state = 1;
                        radix = 10;
                        break;
                    }
                    case 'u':
                    {
                        // unsigned decimal
                        out   = va_get_unsigned(modifiers, &va);
                        sig   = 0;
                        state = 1;
                        radix = 10;
                        break;
                    }
                    case 'o':
                    {
                        // unsigned octal
                        out   = va_get_unsigned(modifiers, &va);
                        sig   = 0;
                        state = 1;
                        radix = 8;
                        break;
                    }
                    case 'x':
                    {
                        // unsigned lowercase hexadecimal
                        out       = va_get_unsigned(modifiers, &va);
                        sig       = 0;
                        state     = 1;
                        radix     = 16;
                        uppercase = 0;
                        break;
                    }
                    case 'b':
                    {
                        // unsigned binary
                        out       = va_get_unsigned(modifiers, &va);
                        sig       = 0;
                        state     = 1;
                        radix     = 2;
                        uppercase = 0;
                        break;
                    }
                    case 'X':
                    {
                        // unsigned uppercase hexadecimal
                        out       = va_get_unsigned(modifiers, &va);
                        sig       = 0;
                        state     = 1;
                        radix     = 16;
                        uppercase = 1;
                        break;
                    }
                    case 'f':
                    {
                        double r = va_arg(va, double);
                        int ir = r;
                        char* buf = calloc(bufsize, 1);
                        if(!flag_dot)
                            precision = 6;
                        if(ir < 0)
                        {
                            ir = -ir;
                            r = -r;
                            neg = 1;
                        }
                        if(ir)
                        {
                            for(k = 0; ir; ++k)
                            {
                                buf[k] = ir % 10 + '0';
                                ir /= 10;
                            }
                        }
                        else
                        {
                            buf[0] = '0';
                            k = 1;
                        }

                        if(!flag_zero)
                        {
                            if(neg)
                            {
                                buf[k] = '-';
                                ++k;
                            }
                            else
                            {
                                if(flag_plus)
                                {
                                    buf[k] = '+';
                                    ++k;
                                }
                                else
                                {
                                    if(flag_space)
                                    {
                                        buf[k] = ' ';
                                        ++k;
                                    }
                                }
                            }
                        }

                        size_t oldk;
                        int64_t padsize = minlen - k - precision - 1;
                        if(neg || flag_plus || (!flag_zero && flag_space))
                        {
                            --padsize;
                        }

                        if(!flag_minus)
                        {
                            oldk = k;
                            for(k = 0; k < padsize; ++k)
                            {
                                if(flag_zero)
                                {
                                    buf[oldk + k] = '0';
                                }
                                else
                                {
                                    buf[oldk + k] = ' ';
                                }
                            }
                            k += oldk;
                        }

                        if(flag_zero)
                        {
                            if(neg)
                            {
                                buf[k] = '-';
                                ++k;
                            }
                            else
                            {
                                if(flag_plus)
                                {
                                    buf[k] = '+';
                                    ++k;
                                }
                            }
                        }

                        oldk = k;
                        if(flag_minus)
                        {
                            for(k = 0; k < padsize; ++k)
                            {
                                if(flag_zero)
                                {
                                    fputc('0', stdout);
                                }
                                else
                                {
                                    fputc(' ', stdout);
                                }
                            }
                        }

                        for(k = 0; k < oldk; ++k)
                        {
                            fputc(buf[oldk - k - 1], stdout);
                        }
                        fputc('.', stream);
                        for(k = 0; k < precision; ++k)
                        {
                            r *= 10;
                            fputc((int)(r) % 10 + '0', stream);
                        }
                        result += oldk + precision;
                        if(padsize > 0)
                            result += padsize;
                        free(buf);
                        state = 2;
                        break;
                    }
                    default:
                    {
                        if(fmt[j] >= '0' && fmt[j] <= '9')
                        {
                            if(flag_dot)
                            {
                                precision = precision * 10 + fmt[j] - '0';
                            }
                            else
                            {
                                minlen = minlen * 10 + fmt[j] - '0';
                            }
                        }
                        else
                        {
                            fputc(fmt[j], stream);
                        }
                    }
                }
            }

            if(state)
                i = j - 1;

            //i += j;
            if(state == 1)
            {
                flag_octo = flag_octo;
                if(minlen >= 64)
                    bufsize = minlen + 1;
                char* buf = calloc(bufsize, 1);

                if(sig && (int64_t)(out) < 0)
                {
                    out = -out;
                    neg = 1;
                }

                if(out)
                {
                    for(k = 0; out; ++k)
                    {
                        buf[k] = out % radix;
                        if(buf[k] > 9)
                        {
                            if(uppercase)
                                buf[k] += 'A' - 10;
                            else
                                buf[k] += 'a' - 10;
                        }
                        else
                        {
                            buf[k] += '0';
                        }
                        out /= radix;
                    }
                }
                else
                {
                    buf[0] = '0';
                    k = 1;
                }

                if(!flag_zero)
                {
                    if(neg)
                    {
                        buf[k] = '-';
                        ++k;
                    }
                    else
                    {
                        if(flag_plus)
                        {
                            buf[k] = '+';
                            ++k;
                        }
                        else
                        {
                            if(flag_space)
                            {
                                buf[k] = ' ';
                                ++k;
                            }
                        }
                    }
                }

                size_t oldk;
                int64_t padsize = minlen - k;
                if(neg || flag_plus || (!flag_zero && flag_space))
                {
                    --padsize;
                }

                if(!flag_minus)
                {
                    oldk = k;
                    for(k = 0; k < padsize; ++k)
                    {
                        if(flag_zero)
                        {
                            buf[oldk + k] = '0';
                        }
                        else
                        {
                            buf[oldk + k] = ' ';
                        }
                    }
                    k += oldk;
                }

                if(flag_zero)
                {
                    if(neg)
                    {
                        buf[k] = '-';
                        ++k;
                    }
                    else
                    {
                        if(flag_plus)
                        {
                            buf[k] = '+';
                            ++k;
                        }
                    }
                }

                oldk = k;
                if(flag_minus)
                {
                    for(k = 0; k < padsize; ++k)
                    {
                        if(flag_zero)
                        {
                            fputc('0', stdout);
                        }
                        else
                        {
                            fputc(' ', stdout);
                        }
                    }
                }

                for(k = 0; k < oldk; ++k)
                {
                    fputc(buf[oldk - k - 1], stdout);
                }
                result += k;
                if(padsize > 0)
                    result += padsize;
                free(buf);
            }
        }
        else
        {
            fputc(fmt[i], stream);
        }
    };
    return result;
}
