#include "../include/vector.h"

#include "../include/stdio.h"
#include "../include/memory.h"

// Basic class operations
vector* vector_new()
{
    vector* this = malloc(sizeof(vector));
    if(this && vector_constructor(this))
        return this;
    return 0;
}

void vector_delete(vector* this)
{
    vector_destructor(this);
    free(this);
}

uint8_t vector_constructor(vector* this)
{
    this->begin = 0;
    this->end   = 0;
    this->alloc = 0;
    this->build = 0;
    this->del   = 0;
    return 1;
}

void    vector_destructor (vector* this)
{
    vector_clear(this);
}

// Methods
void** vector_at(vector* this, size_t num)
{
    if(num < vector_size(this))
    {
        return &(this->begin[num]);
    }
    else
    {
        perr("vector: out of range");
        return 0;
    }
}

void vector_resize(vector* this, size_t size)
{
    if(this->del)
    {
        size_t oldsize = vector_size(this);
        size_t i;
        for(i = size; i < oldsize; ++i)
        {
            this->del(this->begin[i]);
        }
    }
    if(vector_allocated(this) < size)
    {
        vector_realloc(this, size);
    }
    this->end = this->begin + size;
}

void vector_realloc(vector* this, size_t size)
{
    size_t oldsize = vector_size(this);
    this->begin = realloc(this->begin, size);
    if(this->begin)
    {
        this->alloc = this->begin + size;
        this->end = this->begin + oldsize;
        if(size < oldsize)
        {
            this->end = this->begin + size;
        }
    }
    else
    {
        perr("vector: realloc() failed");
    }
}

void vector_clear(vector* this)
{
    void** ptr;
    if(this->del)
    {
        for(ptr = this->begin; ptr != this->end; ++ptr)
        {
            this->del(ptr);
        }
    }
    free(this->begin);
    memset(this, 0, sizeof(vector));
}

size_t vector_size(vector* this)
{
    return (size_t)(this->end - this->begin);
}

size_t vector_allocated(vector* this)
{
    return (size_t)(this->alloc - this->begin);
}

void vector_push_back(vector* this, void* elem)
{
    if(!this->begin)
    {
        vector_realloc(this, 4);
    }
    if(this->end == this->alloc)
    {
        vector_realloc(this, vector_size(this) * 2);
    }
    *(this->end) = elem;
    ++this->end;
}

void vector_pop_back(vector* this)
{
    if(this->end > this->begin)
    {
        if(this->del)
        {
            this->del(this->end - 1);
        }
        --this->end;
    }
    else
    {
        perr("vector: out of range");
    }
}

void vector_erase_by_value(vector* this, void* val)
{
    vector_erase_by_id(this, vector_find(this, val));
}

void vector_erase_by_id(vector* this, size_t i)
{
    size_t n = vector_size(this) - 1;
    uint8_t f = 0;
    for(; i < n; ++i)
    {
        this->begin[i] = this->begin[i + 1];
        f = 1;
    }
    if(f)
    {
        vector_pop_back(this);
    }
}

size_t vector_find(vector* this, void* val)
{
    size_t i;
    size_t n = vector_size(this);
    for(i = 0; i < n; ++i)
    {
        if(this->begin[i] == val)
        {
            break;
        }
    }
    return i;
}
