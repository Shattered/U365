#include <filesystem.h>

#include <stdio.h>
#include <errno.h>

void fs_err()
{
    errno=ENXIO;
    perr("Filesystem is not setup properly");
}

uint8_t fs_create_file(Filesystem* this, const char* name, fs_flags_t flags)
{
    if(this->create_file)
    {
        return this->create_file(this, name, flags);
    }
    else
    {
        fs_err();
        return 0;
    }
}

uint8_t fs_delete_file(Filesystem* this, const char* name)
{
    if(this->create_file)
    {
        return this->delete_file(this, name);
    }
    else
    {
        fs_err();
        return 0;
    }
}

uint8_t fs_create_dir(Filesystem* this, const char* name, fs_flags_t flags)
{
    if(this->create_dir)
    {
        return this->create_dir(this, name, flags);
    }
    else
    {
        fs_err();
        return 0;
    }
}

uint8_t fs_delete_dir(Filesystem* this, const char* name)
{
    if(this->create_file)
    {
        return this->delete_dir(this, name);
    }
    else
    {
        fs_err();
        return 0;
    }
}

void fs_open_file(Filesystem* this, const char* path, const char* mode, FILE* f)
{
    if(this->open_file)
    {
        this->open_file(this, path, mode, f);
    }
    else
    {
        fs_err();
    }
}
