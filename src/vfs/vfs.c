#include "../include/vfs.h"

#include "../include/memory.h"
#include "../include/string.h"
#include "../include/stdio.h"
#include "../include/vesa_fb.h"
#include "../include/tar.h"
#include "../include/debug.h"
#include "../include/arch/i686/devices.h"

uint8_t *initrd_address;
uint8_t vfs_create_file(Filesystem*, const char*, fs_flags_t);
uint8_t vfs_delete_file(Filesystem*, const char*);
uint8_t vfs_create_dir (Filesystem*, const char*, fs_flags_t);
uint8_t vfs_delete_dir (Filesystem*, const char*);
void    vfs_open_file  (Filesystem*, const char*, const char*, FILE*);

Filesystem* root = 0;

char *vfs_test_file =
        "U365 VFS test.\n"
        "If you see this text, U365 VFS works fine.\n"
        "Copyright BPS, 2015-2016. Developers:\n"
        "catnikita255 & k1-801";

char *vfs_license_file =
        "U365 by BPS Dev Team is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.\n"
        "Anybody can use it in their projects with some exceptions.\n"
        "You MUST leave that license text in VFS root (\"/\"), named \"license.txt\".\n"
        "You MUST leave us (the BPS Dev Team) as the original authors. Example:\n"
        "> cat /authors.txt\n"
        "Original Authors    - BPS Dev Team - catnikita255 & k1-801\n"
        "Forked and improved - The FooBar team/company/etc - bsmith145, branf and Neo1975";



char *vfs_os_api_file= "U365 API documentation.\n\n"
                       "Basic STDIO functions\n\n"
                       "printf(const char* fmt, ...)\n"
                       "==============================\n"
                       "Prints formatted string to stdout. All format specifiers are supported.\n"
                       "";
void vfs_set_contents(Filesystem* this, const char* fname, uint8_t *data, int size)
{
    vfs_node* node = vfs_find_node(this, fname);
    if(node)
    {
        ((vfs_fileinfo*)(node->data))->data = data;
        ((vfs_fileinfo*)(node->data))->size = size;
    }
/*
    else
    {
        printf("Node %s not found (does the file does exist?)\n", fname);
    }
*/
}

void init_rootfs()
{
    root = vfs_new();
    /*
    fs_create_dir (root, "bin"     , 0x1FC);
    fs_create_dir (root, "proc"    , 0x1FC);
    fs_create_dir (root, "dev"     , 0x1FC);
    fs_create_dir (root, "run"     , 0x1FC);
    fs_create_dir (root, "sys"     , 0x1FC);
    fs_create_dir (root, "usr"     , 0x1FC);
    fs_create_dir (root, "fonts"   , 0x1FC);

    fs_create_file(root, "/dev/fb0",        0x124); // framebuffer
    //fs_create_file(root, "/dev/mouse",      0x124); // PS/2 mouse.
    //fs_create_file(root, "/dev/kbd",        0x124); // keyboard
    fs_create_file(root, "/fonts/Font.bfn", 0x124); // basic font
    fs_create_file(root, "/test.txt",       0x124);
    fs_create_file(root, "/bmp.bmp",        0x124);
    fs_create_file(root, "/logo.bmp",       0x124); // OS logo
    fs_create_file(root, "/license.txt",    0x124); // license file
    fs_create_file(root, "os_api_doc.txt",  0x124);

    vfs_set_contents(root, "/dev/fb0",        (uint8_t*) srt_info.ScreenFBInfo.framebuffer, srt_info.ScreenFBInfo.height * srt_info.ScreenFBInfo.fbpitch);
    vfs_set_contents(root, "/fonts/Font.bfn", (uint8_t*) file_Font_bfn,                     file_Font_bfn_size);
    //vfs_set_contents(root, "/dev/mouse",      (uint8_t*) malloc(5*4),                       5*4);
    vfs_set_contents(root, "/test.txt",       (uint8_t*) vfs_test_file,                     strlen(vfs_test_file));
    vfs_set_contents(root, "/bmp.bmp",        (uint8_t*) file_bmp_bmp,                      file_bmp_bmp_size);
    vfs_set_contents(root, "/logo.bmp",       (uint8_t*) file_logo_bmp,                     file_logo_bmp_size);
    vfs_set_contents(root, "/license.txt",    (uint8_t*) vfs_license_file,                  strlen(vfs_license_file));
    vfs_set_contents(root, "/os_api_doc.txt", (uint8_t*) vfs_os_api_file,                   strlen(vfs_os_api_file));
    */
    vfs_set_contents(root, "/dev/mouse",      (uint8_t*) malloc(5*4),                       5*4);
    debug_print(NOTICE, "Started initial ram disk unpacking.");
    uint8_t *initrd=initrd_address;
    unsigned int files_num = tar_gethdrcnt(initrd_address);
    debug_print(DEBUG, "Files count detected.");
    tar_hdr_t **hdrs=malloc(files_num*sizeof(tar_hdr_t));
    debug_print(DEBUG, "Header array allocated.");
    unsigned int i=0;
    do
    {
        hdrs[i]=(tar_hdr_t*)initrd;
        /*printf("Name: %s\nSize: %u KB\nUser: %s\nMagic: %s\nModify time: %s\n",
        hdrs[i].name,
        tar_getsize(hdrs[i].size)/1024,
        hdrs[i].uname,
        hdrs[i].magic,
        hdrs[i].mtime);*/
        initrd+=(tar_getsize(hdrs[i]->size)+sizeof(tar_hdr_t));
        i++;
    } while(i<files_num);
    debug_print(DEBUG, "Header array filled successfully.");
    debug_print(NOTICE, "Unpacking process started.");
    //First we need to create ONLY directories.
    for(unsigned int j=0; j < files_num; j++)
    {
        if(hdrs[j]->typeflag=='5') //'5' is "directory" file type
        {
            hdrs[j]->name[strlen(hdrs[j]->name)-1]='\0'; //Cut the last character
            printf("Directory name: %s\n", hdrs[j]->name);
            fs_create_dir(root, hdrs[j]->name,  0x1FC);
        }
    }
    //Then we need to finally create files.
    for(unsigned int k=0; k < files_num; k++)
    {
        if(hdrs[k]->typeflag=='0' || hdrs[k]->typeflag=='\0')
        {
            printf("Creating file from tar, name: %s, size: %d.\n", hdrs[k]->name, tar_getsize(hdrs[k]->size));

            fs_create_file(root, hdrs[k]->name,  0x124);
            vfs_set_contents(root, hdrs[k]->name, ( (uint8_t*) hdrs[k] ) + sizeof(tar_hdr_t), tar_getsize(hdrs[k]->size));
        }
    }
    
}

// VFS
/**
 * Basic class operations
 */
Filesystem* vfs_new()
{
    Filesystem* this = malloc(sizeof(Filesystem));
    if(this && vfs_constructor(this))
        return this;
    return 0;
}

void vfs_delete(Filesystem* this)
{
    if(this)
    {
        vfs_node_delete((vfs_node*)(this->data));
        if(this->parent)
        {
            vector* v = &this->parent->mounted;
            vector_erase_by_value(v, this);
        }
        free(this);
    }
}

uint8_t vfs_constructor(Filesystem* this)
{
    this->data = calloc(sizeof(vfs_node), 1);
    ((vfs_node*)(this->data))->name = "";
    ((vfs_node*)(this->data))->data = calloc(sizeof(vfs_dirinfo), 1);

    this->create_file = &vfs_create_file;
    this->delete_file = &vfs_delete_file;
    this->create_dir  = &vfs_create_dir;
    this->delete_dir  = &vfs_delete_dir;
    this->open_file   = &vfs_open_file;
    return 1;
}

/**
 * Methods
 */
vfs_node* vfs_find_node(Filesystem* this, const char* name)
{
    vfs_node* node = (vfs_node*)(this->data);
    while(name && *name)
    {
        while(*name == '/') ++name;
        size_t i;
        for(i = 0; name[i] && name[i] != '/'; ++i);
        char* buf = (char*)(calloc(i + 1, 1));
        memcpy(buf, name, i);
        name += i;
        vector* v = &((vfs_dirinfo*)(node->data))->nodes;
        node = 0;
        for(i = 0; i < vector_size(v); ++i)
        {
            vfs_node* next = *(vfs_node**)(vector_at(v, i));
            if(!strcmp(next->name, buf))
            {
                node = next;
                break;
            }
        }
        if(!node || (*name && !(node->flags & FS_DIRECTORY)))
        {
            return 0;
        }
    }
    return node;
}

vfs_node* vfs_create_node(Filesystem* this, const char* name, fs_flags_t flags)
{
    size_t i = 0;
    size_t name_len = strlen(name);
    size_t lastsl = 0;
    for(i = 0; i < name_len - 1; ++i)
    {
        if(name[i] == '/')
        {
            lastsl = i + 1;
        }
    }

    char* prefix = calloc(           lastsl + 1, 1);
    char* nname  = calloc(name_len - lastsl + 1, 1);
    memcpy(prefix, name,          lastsl - !!lastsl);
    memcpy(nname,  name + lastsl, name_len - lastsl);

    vfs_node* prenode = vfs_find_node(this, prefix);
    free(prefix);
    if(!prenode)
    {
        free(nname);
        return 0;
    }

    vfs_node* newnode = vfs_node_new();
    if(newnode)
    {
        newnode->name   = nname;
        newnode->flags  = flags;
        newnode->parent = prenode;
        if(flags & FS_DIRECTORY)
        {
            newnode->data = calloc(sizeof(vfs_dirinfo), 1);
            ((vfs_dirinfo*)(newnode->data))->nodes.del = (void(*)(void*))(&vfs_node_delete);
        }
        else
        {
            newnode->data = calloc(sizeof(vfs_fileinfo), 1);
        }
        vector_push_back(&(((vfs_dirinfo*)(prenode->data))->nodes), newnode);
    }
    return newnode;
}

uint8_t vfs_delete_node(Filesystem* this, const char* name)
{
    vfs_node* node = vfs_find_node(this, name);
    if(node)
    {
        vfs_node* prenode = node->parent;
        if(prenode)
        {
            vector* v = &(((vfs_dirinfo*)(prenode->data))->nodes);
            size_t i;
            size_t n = vector_size(v);
            for(i = 0; i < n; ++i)
            {
                if(!strcpy(node->name, (*(vfs_node**)(vector_at(v, i)))->name))
                {
                    break;
                }
            }
            for(; i < n - 1; ++i)
            {
                *vector_at(v, i) = *vector_at(v, i + 1);
            }
            vector_pop_back(v);
        }
    }
    return 0;
}

uint8_t vfs_create_file(Filesystem* this, const char* name, fs_flags_t flags)
{
    return !!vfs_create_node(this, name, flags & (~FS_DIRECTORY));
}

uint8_t vfs_delete_file(Filesystem* this, const char* name)
{
    return vfs_delete_node(this, name);
}

uint8_t vfs_create_dir(Filesystem* this, const char* name, fs_flags_t flags)
{
    return !!vfs_create_node(this, name, flags | FS_DIRECTORY);
}

uint8_t vfs_delete_dir (Filesystem* this, const char* name)
{
    return vfs_delete_node(this, name);
}
