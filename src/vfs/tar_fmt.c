//tar format support in kernel, used for initrd extracting
#include <tar.h>
#include <debug.h>
#include <stdbool.h>
#include <stdint.h>
unsigned int tar_getsize(const char *in) //Copied from OSDev.org, so don't say that I've stolen it.
{
 
    unsigned int size = 0;
    unsigned int j;
    unsigned int count = 1;
 
    for (j = 11; j > 0; j--, count *= 8)
        size += ((in[j - 1] - '0') * count);
 
    return size;
 
}
unsigned int tar_gethdrcnt(void* tar)
{
	debug_print(DEBUG, "Started tar file count calculating.");
	tar_hdr_t *hdr = (tar_hdr_t*) tar;
	int i = 0;
	for(i = 0; hdr->name[0]!='\0'; i++)
	{
		tar+=(tar_getsize(hdr->size)+sizeof(tar_hdr_t));
		hdr = (tar_hdr_t*) tar;
		debug_print(DEBUG, "File found");
	}
	debug_print(DEBUG, "Done, exiting");
	return i;
}