#include "../include/vfs_node.h"

#include "../include/vfs.h"
#include "../include/errno.h"
#include "../include/memory.h"

/**
 * Basic class operations
 */
vfs_node* vfs_node_new()
{
    vfs_node* this = malloc(sizeof(vfs_node));
    if(this && vfs_node_constructor(this))
        return this;
    return 0;
}

void vfs_node_delete(vfs_node* this)
{
    if(this)
    {
        if(vfs_node_is_dir(this))
            vector_destructor(&((vfs_dirinfo*)(this->data))->nodes);
        free(this);
    }
}

uint8_t vfs_node_constructor(vfs_node* this)
{
    this->parent = 0;
    this->name   = 0;
    this->flags  = 0;
    this->data   = 0;
    return 1;
}

/**
 * Methods
 */
uint8_t vfs_node_is_file(const vfs_node* this)
{
    return !(this->flags & FS_DIRECTORY);
}

uint8_t vfs_node_is_dir(const vfs_node* this)
{
    return !!(this->flags & FS_DIRECTORY);
}

size_t vfs_file_get_size(const vfs_node* this)
{
    if(vfs_node_is_file(this))
    {
        return ((vfs_fileinfo*)(this->data))->size;
    }
    return 0;
}

// File stream internal routines

int vfs_file_seek(FILE* stream, long int offset, int origin)
{
    vfs_filestream* fstr = (vfs_filestream*)(stream->data);
    size_t fsize = vfs_file_get_size(fstr->node->data);
    switch(origin)
    {
        case SEEK_SET:
            if(offset >= 0 && (size_t)(offset) <= fsize)
            {
                fstr->pos = offset;
                return 0;
            }
            return EOF;
        case SEEK_CUR:
            if((size_t)(fstr->pos + offset) < fsize)
            {
                fstr->pos += offset;
                return 0;
            }
            return EOF;
        case SEEK_END:
            if(offset <= 0)
            {
                fstr->pos = fsize + offset;
                return 0;
            }
            return EOF;
    }
    return EOF;
}

int16_t vfs_file_getc(FILE* stream)
{
    vfs_filestream* fstr = (vfs_filestream*)(stream->data);
    if(fstr->pos + 1 <= vfs_file_get_size(fstr->node))
    {
        return (int16_t)(uint8_t)(((char*)(((vfs_fileinfo*)(fstr->node->data))->data))[fstr->pos++]);
    }
    return EOF;
}

int16_t vfs_file_putc(FILE* stream, char c)
{
    if(!stream->putc)
    {
        errno=EPERM;
        return EOF;
    }
    vfs_node* node=((vfs_filestream*)(stream->data))->node;
    vfs_filestream* fstr=((vfs_filestream*)(stream->data));
    char* data=(char*)node->data;
    data[++fstr->pos]=c;
    return c;
}

size_t vfs_file_size(FILE* stream)
{
    vfs_filestream* fstr = (vfs_filestream*)(stream->data);
    return vfs_file_get_size(fstr->node);
}

void vfs_open_file(Filesystem* this, const char* name, const char* mode, FILE* stream)
{
    uint8_t r = 0;
    uint8_t w = 0;
    uint8_t a = 0;
    switch(mode[0])
    {
        case 'r': r = 1; break;
        case 'w': w = 1; break;
        case 'a': w = 1; a = 1; break;
    }
    while(*++mode)
    {
        if(*mode == '+')
        {
            r = 1;
            w = 1;
        }
    }

    if(w && !a)
    {
        vfs_delete_node(this, name);
    }

    vfs_node* node = vfs_find_node(this, name);
    if(!node)
    {
        if(!w)
        {
            errno=ENOENT;
            return;
        }
        fs_create_file(this, name, 0x124);
        node = vfs_find_node(this, name);
    }

    if(node)
    {
        memset(stream, 0, sizeof(FILE));
        stream->data = calloc(sizeof(vfs_filestream), 1);
        ((vfs_filestream*)(stream->data))->pos = 0;
        ((vfs_filestream*)(stream->data))->node = node;
        if(r)
            stream->getc = &vfs_file_getc;
        if(w)
        {
            stream->putc = &vfs_file_putc;
            if(a)
            {
                ((vfs_filestream*)(stream->data))->pos = vfs_file_get_size(node);
            }
        }
    }


/*    if(!(node->flags & FS_DIRECTORY))
    {
        if(!node)
        {

        }
        stream->data = calloc(sizeof(vfs_filestream), 1);
        ((vfs_filestream*)(stream->data))->pos = 0;
        ((vfs_filestream*)(stream->data))->node = node;
        stream->seek = &vfs_file_seek;
        stream->size = &vfs_file_size;
        while(*mode)
        {
            switch(*mode)
            {
                case 'r':
                {
                    stream->getc = &vfs_file_getc;
                    break;
                }
                case 'w':
                {
                    if(!(node->flags & FS_OTHER_W))
                    {
                        perr("File is read-only\n");
                        return;
                    }
                    stream->putc = &vfs_file_putc;
                    free(((vfs_fileinfo*)(node->data))->data);
                    // Create a new memory zone 
                    ((vfs_fileinfo*)(node->data))->size = 0;
                    ((vfs_fileinfo*)(node->data))->data = malloc(0);
                    break;
                }
                case 'a':
                {
                    stream->putc = &vfs_file_putc;
                    stream->pos  = vfs_file_get_size(node);
                    break;
                }
                case '+':
                {
                    stream->putc = &vfs_file_putc;
                    stream->getc = &vfs_file_getc;
                    break;
                }
            }
            ++mode;
        }
    }*/
}
