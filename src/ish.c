#include <ish.h>

#include "../include/cmds.h"
#include "../include/stdio.h"
#include "../include/panic.h"
#include "../include/string.h"
#include "../include/task.h"

// Top-level environment variables
env_descriptor* env;

command_table_t cmds[MAX_COMMANDS];
int cmdNum=0;
void print_ps1()
{
    const char *ps1=env_get(env, "PS1");
    int i=0;
    while(ps1[i]!='\0')
    {
        if(ps1[i]!='\\')
            putchar(ps1[i]);
        else
        {
            switch(ps1[i+1])
            {
                case 'h': printf("%s", env_get(env, "HOSTNAME")); break;
                case 'u': printf("%s", env_get(env, "USER")); break;
                case '$': printf("%c", (strcmp(env_get(env, "UID"), "0")==0) ? '#' : '$' ); break;
                case 'w': printf("%s", env_get(env, "PWD")); break;
            }
            i++;
        }
        i++;
    }
}
void noSuchCmd(const char *cmd)
{
    printf("ish: no such command: %s", cmd);
}

void addCmd(const char *name, const char *desc, void (*fcn)())
{
    if(cmdNum + 1 < MAX_COMMANDS)
    {
        strcpy(cmds[cmdNum].name,        name);
        strcpy(cmds[cmdNum].description, desc);
        cmds[cmdNum].action=fcn;
        ++cmdNum;
    }
}

uint8_t parseCmd(int argc, char **argv)
{
    if(!strcmp(argv[0], ""))
    {
        return 0;
    }

    for(int i = 0; i < cmdNum; i++)
    {
        if(strcmp(cmds[i].name, argv[0]) == 0)
        {
            void (*cmd_run)(int,char**) = cmds[i].action;
            cmd_run(argc, argv);
            return 0;
        }
    }
    return 1;
}

size_t argsize = 256;

int basreg(const char* text, char*** buf)
{
    uint8_t nbuf = 1;
    size_t i, j;
    int result = 0;
    char brackets = 0;
    *buf = 0;
    for(i = 0; text[i]; ++i)
    {
        if(nbuf)
        {
            ++result;
            *buf = (char**)(realloc(*buf, result * sizeof(char*)));
            (*buf)[result - 1] = (char*)(calloc(argsize, 1));
            j = 0;
            nbuf = 0;
        }
        if(text[i] == '\\' && text[i + 1])
        {
            switch(text[i + 1])
            {
                case '0':
                    (*buf)[result - 1][j++] = 0;
                    break;
                case 'n':
                    (*buf)[result - 1][j++] = '\n';
                    break;
                case 't':
                    (*buf)[result - 1][j++] = '\t';
                    break;
                default:
                    (*buf)[result - 1][j++] = text[i + 1];
            }
            ++i;
        }
        else
        {
            if(text[i] == '"' || text[i] == '\'')
            {
                if(!brackets)
                    brackets = text[i];
                else
                    if(brackets == text[i])
                        brackets = 0;
            }
            if(!brackets && (text[i] == ' ' || text[i] == '\t' || text[i] == '\n'))
            {
                nbuf = 1;
            }
            else
            {
                (*buf)[result - 1][j++] = text[i];
            }
        }
    }
    return result;
}

void ishMain()
{
    addCmd("\n",       "Empty command",                              emptyCmd);
    addCmd("help",     "Display known commands",                     help);
    addCmd("sysinfo",  "System info (only with SMBIOS)",             sysinfo);
    addCmd("ascii",    "Display ASCII table.",                       atable);
    addCmd("gugdun",   "Easter egg",                                 kotiki);
    addCmd("panic",    "Panic",                                      gpf);
    addCmd("smbios",   "SMBIOS Area Contents",                       smb_data);
    addCmd("testvfs",  "VFS test",                                   cmd_vfs_test);
    addCmd("showlogo", "show U365 logo in the middle of the screen", showlogo);
    addCmd("ish2",     "Run next generation Integrated Shell",       ish2);
    addCmd("cat",      "Show contents of specified files",           cmd_cat);
    addCmd("echo",     "Output all passed arguments",                cmd_echo);
    addCmd("reboot",   "Reboot your PC.",                            cmd_reboot);
    addCmd("date",     "Show system clock date and time.",           cmd_date);
    addCmd("bfntest",  "Test the BFN rendering system",              bfntest);
    addCmd("surface",  "Test the screen surface",                    surface_test);
    addCmd("mouse",    "Test PS/2 mouse driver",                     mouse_test);
    addCmd("lscpu",    "CPU information. Code isn't mine!",          cmd_lscpu);
    addCmd("errno",    "Test the errno and strerror.",               cmd_errno_test);
    addCmd("systest",  "Full system test",                           cmd_all_test);
    while(1)
    {
        printf("\nish1.0 : > ");
        kbd_waitForBufToEmpty();
        char* input=malloc(128);
        kbd_gets(input);
        int argc;
        char** argv=malloc(256);
        argc = basreg(input, &argv);
        printf("\n");
        if(parseCmd(argc, argv)==1)
            noSuchCmd(argv[0]);
    }
}

void ish2()
{
    static uint8_t ready = 0;
    if(!ready)
    {
        env = calloc(sizeof(env_descriptor), 0);
        env->parent = 0;
        env->tree = string_tree_new();
        env_set(env, "USER",     "root");
        env_set(env, "HOSTNAME", "localhost");
        env_set(env, "UID",      "0");
        env_set(env, "GID",      "0");
        env_set(env, "PWD",      "/");
        env_set(env, "HOME",     "/root");
        env_set(env, "PATH",     "/bin:/sbin:/usr/bin:/usr/sbin");
        env_set(env, "PS1",      "\\u@\\h : \\w \\$ ");

/*        printf("USER    =%s\n"
               "HOSTNAME=%s\n"
               "UID     =%s\n"
               "GID     =%s\n"
               "PWD     =%s\n"
               "HOME    =%s\n"
               "PS      =%s\n\n", env_get(env, "USER"), env_get(env, "HOSTNAME"), env_get(env, "UID"), env_get(env, "GID"), env_get(env, "PWD"), env_get(env, "HOME"), env_get(env, "PS"));*/
    }
    while(1)
    {
        print_ps1();
        char* input=malloc(128);
        kbd_gets(input);
        printf("\n");
        int argc;
        char** argv=malloc(256);
        argc = basreg(input, &argv);
        if(!strcmp(argv[0], "exit"))
            return;
        if(parseCmd(argc, argv) == 1)
        {
            printf("Debug: no such command: %s", argv);
        }
        printf("\n");
        /*
        for(int i = 0; i < argc; ++i)
        {
            free(argv[i]);
        }
        free(argv);*/
        
    }
}

const char* env_get(env_descriptor* this, const char* key)
{
    const char* result = string_tree_get(this->tree, key);
    if(!result && this->parent)
        return env_get(this->parent, key);
    return result;
}

void env_set(env_descriptor* this, const char* key, const char* value)
{
    string_tree_set(this->tree, key, value);
}
