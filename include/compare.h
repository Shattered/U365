#ifndef __COMPARE_H
#define __COMPARE_H
#define max(a,b) (a>b) ? a : b
#define min(a,b) (a<b) ? a : b
#define abs(x) (x<0) ? -x : x
#define _QUOT(x) #x
#define QUOT(x) _QUOT(x)
#endif