#ifndef FIO_H
#define FIO_H

#include <stdarg.h>
#include <stdint.h>
#include <stddef.h>
#ifndef EOF
#define EOF -1
#define SEEK_SET -1
#define SEEK_CUR 0
#define SEEK_END 1
#endif

typedef struct FILE FILE;

struct FILE
{
    void** data; // IU

    int     (*seek)(FILE*, long int, int);
    int16_t (*getc)(FILE*);
    int16_t (*putc)(FILE*, char);
    size_t  (*gets)(FILE*, char*, size_t);
    size_t  (*puts)(FILE*, const char*);
    size_t  (*size)(FILE*);
    void    (*close)(FILE*);
};

FILE*   fopen  (const char*, const char*);
void    freopen(const char*, const char*, FILE*);
int16_t fgetc  (FILE*);
int16_t fputc  (char, FILE*);
size_t  fgets  (char*, size_t, FILE*);
size_t  fputs  (const char*, FILE*);
void    fclose (FILE*);
size_t  fread  (void*, size_t, size_t, FILE*);
int     fseek  (FILE*, long int, int);

int  fprintf(FILE*, const char*, ...);
int vfprintf(FILE*, const char*, va_list);

void perr(const char*);

#endif // FIO_H
