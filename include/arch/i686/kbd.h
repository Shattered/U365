#ifndef KBD_H
#define KBD_H

#include <stdint.h>
#include <stddef.h>

#include "../../../include/arch/i686/io.h"
#include "../../../include/arch/i686/devices.h"

#include "../../../include/stdio.h"
#include "../../../include/tty_old.h"
#include "../../../include/string.h"
#include "../../../include/memory.h"
#define kbd_scanToChr(c) scancode[c]
/*
Something about this header.
It's a internal low-level header, used by the keyboard system. Do not include it in your files; use scrio.h instead.
*/

//Scancode table to get codes corresponding to each key. All ^s are non-printable characters as arrows, F* etc.
extern unsigned char scancode[256];

#define KBD_DATA 0x60

uint8_t     kbd_getScancode(); //Reading from KBD_DATA.
uint8_t     kbd_getchar();
size_t      kbd_gets(char* s);
size_t      kbd_getsn(char*, size_t);
void        kbd_waitForBufToEmpty();
void        kbd_reset_cpu();
void	 	kbd_scancodes_setup();

#endif
