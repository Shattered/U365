#ifndef __TTY_H
#define __TTY_H

#include <stdint.h>
#include "vesa_fb.h"

int tty_w;
int tty_h;
int tty_x, tty_y;
uint8_t tty_color;
extern uint8_t *bfn_437;
extern uint16_t *tty_buf;

char * font_data[256]; // DEPRECATED

void tty_putentryat(char, int, int, uint8_t);
void setupFonts();
void initTTY();
void vesa_putc(char, int, int, uint32_t, uint32_t);
void tty_putchar(char);
void tty_setcolor(uint8_t);
uint8_t tty_mkcolor(uint8_t, uint8_t);
void tty_wrstr(const char *);
void tty_goToPos(int, int);

#endif
