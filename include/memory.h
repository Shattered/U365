#ifndef __MEM_H
#define __MEM_H

#include <stdint.h>
#include <stddef.h>

void* memcpy (void*, const void*, size_t);
void* memset (void*, uint8_t, size_t);
void* memmove(void*, void*, size_t);
// Old heap
void* malloc (size_t);
void* calloc (size_t, size_t);
void* realloc(void*, size_t);
void  free   (void*);

#endif
