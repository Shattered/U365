#ifndef SURFACE_H
#define SURFACE_H

#include <stdint.h>

typedef struct rect       rect;
typedef struct color_rgba color_rgba;
typedef struct surface    surface;

typedef enum
{
    SURFACE_READABLE  = 0x0001,
    SURFACE_RESIZABLE = 0x0002,
    SURFACE_EDITABLE  = 0x0004,
    SURFACE_ALPHA     = 0x0008,
} surface_flags_t;

#define SURFACE_DEFAULT_FLAGS (SURFACE_READABLE | SURFACE_RESIZABLE | SURFACE_EDITABLE | SURFACE_ALPHA)

struct rect
{
    uint32_t x;
    uint32_t y;
    uint32_t w;
    uint32_t h;
};

struct color_rgba
{
    uint8_t r;
    uint8_t g;
    uint8_t b;
    uint8_t a;
};

struct surface
{
    void** data;
    uint32_t w;
    uint32_t h;
    surface_flags_t flags;

    uint8_t rb;
    uint8_t gb;
    uint8_t bb;
    uint8_t ab;
    uint8_t bpp;

    void (*del)(surface*);
};

/**
 * Basic colors for fast access
 */
extern const color_rgba color_transparency; // 0x00000000
extern const color_rgba color_black;        // 0x000000FF
extern const color_rgba color_red;          // 0xFF0000FF
extern const color_rgba color_green;        // 0x00FF00FF
extern const color_rgba color_blue;         // 0x0000FFFF
extern const color_rgba color_cyan;         // 0x00FFFFFF
extern const color_rgba color_magenta;      // 0xFF00FFFF
extern const color_rgba color_yellow;       // 0xFFFF00FF
extern const color_rgba color_white;        // 0xFFFFFFFF
extern surface* surface_screen;

// Color operations
color_rgba color_split(color_rgba, color_rgba);
color_rgba color_uint32_to_rgba(uint32_t);
uint32_t   color_rgba_to_uint32(color_rgba);

/**
 * Basic class operations
 */
surface* surface_new();
void     surface_delete     (surface*);
uint8_t  surface_constructor(surface*);

/**
 * Methods
 */
surface* surface_copy       (const surface*);
uint8_t  surface_apply      (      surface*, const surface*, rect);
surface* surface_extract    (const surface*, rect);
uint8_t  surface_fill_rect  (      surface*, color_rgba, rect);
uint8_t  surface_resize     (      surface*, uint32_t, uint32_t);
uint8_t  surface_set_pixel  (      surface*, color_rgba, uint32_t, uint32_t);
color_rgba surface_get_pixel(const surface*, uint32_t, uint32_t);

// Default destructor to be set which will free all data allocated for image holding
void surface_default_destructor(surface*);
// Initialize all data fields for ordinary RGBA image holding
void surface_default_init(surface*, uint32_t, uint32_t);
// Get the screen surface (Unresizable and should be reinitialized when the screen is resized)
void init_surface_screen();

#endif // SURFACE_H
