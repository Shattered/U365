#ifndef FILESYSTEM_H
#define FILESYSTEM_H

#include <stddef.h>
#include <stdint.h>

typedef uint32_t fs_node_id_t;
typedef struct Filesystem Filesystem;

#include "../include/vector.h"
#include "../include/fio.h"

typedef enum
{
    FS_DIRECTORY = 0x1000,

    FS_SGID      = 0x0800,
    FS_SUID      = 0x0400,

    FS_STICKY    = 0x0200,

    FS_USER_R    = 0x0100,
    FS_USER_W    = 0x0080,
    FS_USER_X    = 0x0040,

    FS_GROUP_R   = 0x0020,
    FS_GROUP_W   = 0x0010,
    FS_GROUP_X   = 0x0008,

    FS_OTHER_R   = 0x0004,
    FS_OTHER_W   = 0x0002,
    FS_OTHER_X   = 0x0001,
} fs_flags_t;

extern Filesystem* root;

struct Filesystem
{
    Filesystem* parent;
    const char* offset;
    vector mounted;

    void** data; // IU

    uint8_t (*create_file)(Filesystem*, const char*, fs_flags_t);
    uint8_t (*delete_file)(Filesystem*, const char*);
    uint8_t (*create_dir) (Filesystem*, const char*, fs_flags_t);
    uint8_t (*delete_dir) (Filesystem*, const char*);
    size_t  (*get_next)   (Filesystem*, const char*);
    void    (*open_file)  (Filesystem*, const char*, const char*, FILE*);
    void    (*del)        (Filesystem*);
};

uint8_t fs_constructor(Filesystem*);
void    fs_delete     (Filesystem*);

uint8_t fs_mount      (Filesystem*, Filesystem*, const char*);
uint8_t fs_umount     (Filesystem*);
uint8_t fs_create_file(Filesystem*, const char*, fs_flags_t);
uint8_t fs_delete_file(Filesystem*, const char*);
uint8_t fs_create_dir (Filesystem*, const char*, fs_flags_t);
uint8_t fs_delete_dir (Filesystem*, const char*);
void    fs_open_file  (Filesystem*, const char*, const char*, FILE*);

#endif // FILESYSTEM_H
