#ifndef __STRING_H
#define __STRING_H

#include <stddef.h>

int    strcmp(const char*, const char*);
char*  strcpy(char*, const char*);
size_t strlen(const char*);
char*  strrev(char*);

int   atoi(const char*);
char* itoa(int, char*, int);
int   memcmp(const void*, const void*, size_t);

#endif