#ifndef __DRAWING_H
#define __DRAWING_H
#include <stdint.h>
#include <surface.h>
void hLine(int x, int y, int length, uint32_t color);
void vLine(int x, int y, int length, uint32_t color);
void fillRect(int,int,int,int,uint32_t);
void putLine( int , int, int , int , color_rgba  );
void drawBitmap(int,int,uint32_t[],int,int);
#endif