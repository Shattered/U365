#ifndef ERRNO_H
#define ERRNO_H
extern int errno;
enum {ENOENT=1, ENXIO, EROFS, ENOMEM, EACCES, EPERM};
const char* strerror(int);
#endif //ERRNO_H