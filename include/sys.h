#ifndef __SYS_H
#define __SYS_H

#include <stdbool.h>
#include <stdint.h>

//#define NOT_FOR_RELEASE 1

void arch_init();
uint16_t getCPUFreq();
extern bool        __u365_init_done    ;
extern const char* machine_bios_vendor ;
extern const char* machine_bios_version;
extern const char* machine_bios_bdate  ;

struct PCDevice
{
    int device_id; //Device unique ID.
    int device_irq; //Optional. For devices using IRQs.
    uint8_t bool_values[256]; //Like mouse buttons clicked/released or something
    int      num_values[256]; //Like keyboard scancodes/keycodes, scancode set currently using or mouse X/Y coords. Any number values/
};

struct SystemRTInfo
{
    struct
    {
        uint8_t *framebuffer;
        uint32_t pixbuf[2048][2048];
        int height, width;
        uint32_t fbpitch;
        uint8_t  fbbpp;
    } ScreenFBInfo;
    uint64_t devices_num;
    struct PCDevice installed_devices[1024];
};
int detect_cpu();
extern struct SystemRTInfo srt_info;
#endif
