#ifndef VFS_H
#define VFS_H

#include <stdint.h>
#include <stddef.h>

typedef struct vfs_dirinfo    vfs_dirinfo;
typedef struct vfs_fileinfo   vfs_fileinfo;
typedef struct vfs_filestream vfs_filestream;

#include "filesystem.h"
#include "vector.h"
#include "vfs_node.h"

extern Filesystem* root;

struct vfs_dirinfo
{
    vector nodes;
};

struct vfs_fileinfo
{
    size_t size;
    void* data;
};

struct vfs_filestream
{
    vfs_node* node;
    size_t pos;
};

void init_rootfs();

/**
 * Basic class operations
 */
Filesystem* vfs_new();
void        vfs_delete();
uint8_t     vfs_constructor(Filesystem*);

/**
 * Methods
 */
vfs_node*   vfs_create_node(Filesystem*, const char*, fs_flags_t);
uint8_t     vfs_delete_node(Filesystem*, const char*);
vfs_node*   vfs_find_node  (Filesystem*, const char*);
extern uint8_t *initrd_address;

#endif //VFS_H
