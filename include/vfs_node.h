#ifndef VFS_NODE_H
#define VFS_NODE_H

#include <stdint.h>
#include <stddef.h>

typedef struct vfs_node       vfs_node;

#include "filesystem.h"

struct vfs_node
{
    vfs_node*  parent;
    char*      name;
    fs_flags_t flags;
    void*      data;
};

/**
 * Basic class operations
 */
vfs_node* vfs_node_new();
void      vfs_node_delete     (vfs_node*);
uint8_t   vfs_node_constructor(vfs_node*);

/**
 * Methods
 */
uint8_t   vfs_node_is_file    (const vfs_node*);
uint8_t   vfs_node_is_dir     (const vfs_node*);
size_t    vfs_file_get_size   (const vfs_node*);

#endif // VFS_NODE_H
