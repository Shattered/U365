#ifndef FONT_H
#define FONT_H

#include <stdint.h>

typedef struct font font;

#include "../include/glyph.h"

struct font
{
    glyph* g;
    size_t n;
    uint32_t w;
    uint32_t h;
};

/**
 * Basic class operations
 */
font*    font_new();
void     font_delete     (font*);
uint8_t  font_constructor(font*);
void     font_destructor (font*);

/**
 * Methods
 */
font*    font_open(FILE*);
surface* font_draw_unicode(font*, uint32_t*, color_rgba);
surface* font_draw_utf8   (font*, const char*, color_rgba);
size_t   utf8_convert(const char*, uint32_t*);
surface* bfn_write_str(const char* s, color_rgba color);

#endif // FONT_H
