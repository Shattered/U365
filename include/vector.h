#ifndef VECTOR_H
#define VECTOR_H

#include <stdint.h>
#include <stddef.h>

typedef struct vector_entry vector_entry;
typedef struct vector vector;

struct vector
{
    void** begin;
    void** end;
    void** alloc;
    void*  (*build)();
    void   (*del  )(void*);
};

// Basic class operations
vector* vector_new();
void    vector_delete        (vector*);
uint8_t vector_constructor   (vector*);
void    vector_destructor    (vector*);

// Methods
void**  vector_at            (vector*, size_t);
void    vector_resize        (vector*, size_t);
void    vector_realloc       (vector*, size_t);
void    vector_clear         (vector*);
void    vector_push_back     (vector*, void*);
void    vector_pop_back      (vector*);
size_t  vector_size          (vector*);
size_t  vector_allocated     (vector*);
void    vector_erase_by_value(vector*, void*);
void    vector_erase_by_id   (vector*, size_t);
size_t  vector_find          (vector*, void*);

#endif
