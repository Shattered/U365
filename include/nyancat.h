#define WHITE_COLOR 0xFFFFFF
#define FWHITE_COLOR 0xFFFFFE
#define BLACK_COLOR 0x000000
#define PINK_COLOR 0xEB91E5
#define GREY_COLOR 0x808080
#define DOTS_COLOR 0xE38686
#define BODY_COLOR 0xC5D49D

#define RED_COLOR 0xFF0000
#define ORANGE_COLOR 0xFF8800
#define YELLOW_COLOR 0xFFFF00
#define GREEN_COLOR 0x16A82C
#define BLUE_COLOR 0x1CB3FF
#define DBLUE_COLOR 0x3E32E3

#ifndef _NYANCAT_H
#define _NYANCAT_H

#include <stdint.h>
#include "../include/vesa_fb.h"

void start_bench();

#endif
