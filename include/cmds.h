#ifndef CMDS_H
#define CMDS_H

void help(int,char**);
void sysinfo(int,char**);
void atable();
void emptyCmd();
void kotiki();
void gpf();
void smb_data();
void cmd_cat(int,char**);
void cmd_date();
void cmd_lscpu();
void cmd_echo(int, char**);
void cmd_vfs_test();
void cmd_errno_test();
void cmd_all_test();
void cmd_reboot();
void showlogo();
void ish2();
void bfntest();
void surface_test();
void mouse_test();

#endif // CMDS_H