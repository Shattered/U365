#ifndef __DEBUG_H
#define __DEBUG_H
#define quot(x) #x
#define assert(check) do {\
 if(!(check))\
 	{\
 	 debug_print(FATAL, "Assertion failed: line " QUOT( __LINE__ ) ", file " __FILE__ ", compile time  " __DATE__ " " __TIME__ ". Crashing." );\
 	 for(;;);\
 	 }\
 	} while(0) 
enum fatal_err   {
	ENOTCOMPAT              ,
	EACPI_RSDP_SIG          ,
	EACPI_RSDP_CHECKSUM     ,
	EACPI_RSDT_INVALID      ,
	EACPI_FADT_NOT_FOUND    ,
	EACPI_DSDT_INVALID      ,
	EACPI_DSDT_NOFADT       ,
	EACPI_ENABLE_ERROR      ,
	EACPI_RSDP_CHKSM_INVALID,
	EACPI_DSDT_NOS5         ,
	EBOOT_MAGIC_ERROR       ,
	ENO_INITRD              ,
	EKMAIN_EXITED           ,
	EINCOMPATIBLE_BOOTLOADER,
};
enum dbg_msg_lvl {NOTICE, INIT, DEBUG, SUCCESS, WARNING, FATAL, ERROR, PANIC};


#include <stdint.h>
void debug_print(int, const char*);
void fatal_error(int);

#endif