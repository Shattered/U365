U365 by BPS Dev Team is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.
Anybody can use it in their projects with some exceptions.
You MUST leave that license text in VFS root ("/"), named "license.txt".
You MUST leave us (the BPS Dev Team) as the original authors. Example:
> cat /authors.txt
Original Authors    - BPS Dev Team - catnikita255 & k1-801
Forked and improved - The FooBar team/company/etc - bsmith145, branf and Neo1975
