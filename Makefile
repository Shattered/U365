ARROW=[1;34m==>[1;37m

#U365 Makefile configuration

LSCRIPT    = link
BUILD_DIR  = build
SRC_DIR    = src
INC_DIR    = include
VFS_DIR    = vfs
OS_VER     = 0.7

#Filenames

ARCH       = i686
BINFORMAT  = elf
BITS       = 32
ISODIR     = $(BUILD_DIR)/iso/fs
GRUB_BIN   = /usr/lib/grub/i386-pc/
CROSS      = ~/opt/cross
CROSSBIN   = $(CROSS)/bin/
GAS        = $(CROSSBIN)$(ARCH)-$(BINFORMAT)-as #AT&T-syntax assembler
CC         = $(CROSSBIN)$(ARCH)-$(BINFORMAT)-gcc
IASM       = nasm #Intel-syntax assembler
ISOFILE    = u365-$(OS_VER)#Compiled ELF binary and ISO image name.
BINFILE    = u365
EMU        = qemu-system-i386
EFLAGS     = -cdrom $(BUILD_DIR)/releases/$(ISOFILE).iso -m 128
DEFS       = -DOSVER="$(OS_VER)"
MKRSCFLAGS = -d $(GRUB_BIN) -o $(BUILD_DIR)/releases/$(ISOFILE).iso $(ISODIR)
CFLAGS     = -O3 -ffreestanding -Wall -Wextra -Wmaybe-uninitialized -fno-exceptions -std=gnu11 -Iinclude -c $(DEFS) -Wtype-limits
GASFLAGS   =
IASFLAGS   = -f $(BINFORMAT)
LDFLAGS    = -T $(LSCRIPT).ld -o $(BUILD_DIR)/bin/$(BINFILE).$(BINFORMAT) -O3 -nostdlib -lgcc

# Source file names computing
# Multiboot header first
SOURCES  := $(SRC_DIR)/arch/$(ARCH)/boot.asm
# Al common sources
SOURCES  += $(shell find $(SRC_DIR) -name "*.c"   -and -not -path "$(SRC_DIR)/arch/*"  -type f -print)
SOURCES  += $(shell find $(SRC_DIR) -name "*.s"   -and -not -path "$(SRC_DIR)/arch/*" -type f -print)
SOURCES  += $(shell find $(SRC_DIR) -name "*.asm" -and -not -path "$(SRC_DIR)/arch/*" -type f -print)
# Architecture-dependent sources
SOURCES  += $(shell find $(SRC_DIR)/arch/$(ARCH) -name '*.c' -type f -print)
SOURCES  += $(shell find $(SRC_DIR)/arch/$(ARCH) -name '*.s' -type f -print)
SOURCES  += $(shell find $(SRC_DIR)/arch/$(ARCH) -name '*.asm' -and -not -path "$(SRC_DIR)/arch/$(ARCH)/boot.asm" -type f -print)

SRCDIRS  := $(shell find $(SRC_DIR) -type d -print)
VFSDIRS  := $(shell find $(VFS_DIR) -type d -print)
OBJDIRS  := $(patsubst $(SRC_DIR)/%,"$(BUILD_DIR)/obj/%",$(SRCDIRS))
CFSDIRS  := $(patsubst $(VFS_DIR)/tree/%,"$(VFS_DIR)/conv/%",$(VFSDIRS))

FAKE := $(shell echo $(SOURCES))
# Object file names computing
OBJS     := $(patsubst $(SRC_DIR)/%.c,"$(BUILD_DIR)/obj/%.c.o",$(SOURCES))
#OBJS     := $(patsubst $(SRC_DIR)/libc/%.c,"$(BUILD_DIR)/obj/libc/%.c.o",$(SOURCES))
OBJS     := $(patsubst $(SRC_DIR)/%.s,"$(BUILD_DIR)/obj/%.s.o",$(OBJS))
OBJS     := $(patsubst $(SRC_DIR)/%.asm,"$(BUILD_DIR)/obj/%.asm.o",$(OBJS))

#End

# target      _welcome dependencies
all:          _welcome clean directories compile link initrd iso run
	@echo -n

# Welcome user at make call
_welcome:
	@echo " [1;33mMakefile:[0m [1;32mU365[0m"

compile:     _welcome clean directories _compile $(SOURCES)
#	@echo -e " $(ARROW) Compiling GDT[0m"
#	@$(IASM) $(SRC_DIR)/arch/$(ARCH)/gdt.s -o $(BUILD_DIR)/obj/gdt.o $(IASFLAGS)
#	@echo -e " $(ARROW) Compiling IDT[0m"
#	@$(IASM) $(SRC_DIR)/arch/$(ARCH)/idt.s -o $(BUILD_DIR)/obj/idt.o $(IASFLAGS)
#	@echo -e " $(ARROW) Compiling C sources[0m"
#	@$(CC) $(SOURCES) $(SRC_DIR)/arch/$(ARCH)/init.c $(IOBJS) $(CFLAGS) $(LDFLAGS)
	@echo -n
initrd: 	_welcome clean directories
	@echo " $(ARROW) Generating initrd[0m"
	@cd initrd; tar -cf ../build/iso/fs/boot/initrd.tar *
	

link:        _welcome clean directories compile
	@echo " $(ARROW) Linking[0m"
	@$(CC) $(OBJS) $(LDFLAGS)


iso:         _welcome directories
	@echo " $(ARROW) Generating an ISO image[0m"

	@echo "insmod gfxterm;  \
insmod vbe;  \
timeout=5;   \
set gfxmode=1024x768;  \
menuentry \"U365 Basic System 1.0\"  \
{  \
	echo 'Loading kernel...';            \
	multiboot /boot/u365.elf;            \
	echo 'Loading initrd...'            ;\
	module    /boot/initrd.tar  initrd;  \
	boot;  \
}  \
" > $(BUILD_DIR)/iso/fs/grub.cfg

	@cp $(BUILD_DIR)/iso/fs/grub.cfg $(BUILD_DIR)/iso/fs/boot/grub/grub.cfg
	@cp $(BUILD_DIR)/bin/$(BINFILE).$(BINFORMAT) $(BUILD_DIR)/iso/fs/boot/
	@grub-mkrescue $(MKRSCFLAGS) &> /dev/null
	@echo -n

run:         _welcome
	@echo " $(ARROW) Booting the ISO image[0m"
	$(EMU) $(EFLAGS)
	@echo -n

clean:       _welcome
	@echo " $(ARROW) Cleaning[0m"
	@rm -rf $(BUILD_DIR) $(VFS_DIR)/conv *.iso initrd.tar
	@echo -n

directories: _welcome
	@echo " $(ARROW) Creating build directories[0m"
	@mkdir -p $(OBJDIRS) $(CFSDIRS) $(BUILD_DIR)/bin $(BUILD_DIR)/iso/fs/boot $(BUILD_DIR)/iso/fs/boot/grub $(BUILD_DIR)/iso/fs/fonts $(BUILD_DIR)/releases
	@echo -n

Makefile:
	@echo " [1;31mStrange make bug prevented[0m"

# Compilation notification - do not remove
_compile:
	@echo " $(ARROW) Compiling[0m"
# Compilation routines
%.c:          _welcome directories _compile
	@echo " [0;32m Building C file:[0m [1;32m$@[0m"
	@$(CC) $@ -o $(patsubst $(SRC_DIR)/%.c,$(BUILD_DIR)/obj/%.c.o,$@) $(CFLAGS)
	@echo -n

%.s:          _welcome directories _compile
	@echo " [0;32m Building GAS file:[0m [1;32m$@[0m"
	@$(GAS) $@ -o $(patsubst $(SRC_DIR)/%.s,$(BUILD_DIR)/obj/%.s.o,$@)
	@echo -n

%.asm:        _welcome directories _compile
	@echo " [0;32m Building IAS file:[0m [1;32m$@[0m"
	@$(IASM) $@ -o $(patsubst $(SRC_DIR)/%.asm,$(BUILD_DIR)/obj/%.asm.o,$@) $(IASFLAGS)
	@echo -n
